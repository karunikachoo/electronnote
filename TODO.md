// TODO: 

- Add markdown reference guide
- Note on close refresh db
- Add Quote block
- Add right click context menu
- Add save workspace "Notespace"
- Make reopened notes follow workspace locations
- Add Animation Support
- Add Shapes
- Add Mouse Move dead zone
- ~~Add example note to be added on first creation~~
- ~~Add Save button~~
- ~~horizontal scroll code block~~
- ~~Add link option for external~~
- ~~Add external link support~~
- ~~Add Home/End key support~~
- ~~Add Scroll to support~~
- ~~Add Image Copy & Paste~~
- ~~Add Zoom slider~~