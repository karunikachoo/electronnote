import {surface} from "../canvas/surface";
import {ddraw} from "../canvas/dynamicDraw";
import {SurfaceConst} from "../canvas/surfaceConst";


export class ZoomSlider {
    id: string;
    c: HTMLCanvasElement;

    value: number = 1;

    constructor() {
        this.onDown = this.onDown.bind(this);
        this.onMove = this.onMove.bind(this);
        this.onUp = this.onUp.bind(this);
    }

    init() {
        this.id = 'zoom-slider';
        this.c = document.getElementById(this.id) as HTMLCanvasElement;

        this.c.onmousedown = this.onDown;
        this.c.onmousemove = this.onMove;
        this.c.onmouseup = this.onUp;
        this.c.onmouseleave = (evt) => {
            this.mouseDown = false;
        }
        this.c.ondblclick = (evt) => {
            this.onZoomSliderUpdate(1);
            this.draw();
        }
        this.c.onwheel = (evt) => {
            const dy = evt.deltaY * 0.0005;
            this.onZoomSliderUpdate(surface.scale + dy);
            this.draw();
        }

        this.updateSize();
    }

    onZoomSliderUpdate(value: number) {
        surface.setScale(value, surface.c.width/2, surface.c.height/2);
    }

    draw() {
        const ctx = this.c.getContext('2d');
        ctx.clearRect(0, 0, this.c.width, this.c.height);

        const y = this._computeY(surface.scale);
        const rect = this.c.getBoundingClientRect();

        ctx.fillStyle = "#ffffffaa";
        ctx.beginPath();
        ctx.rect(0, y, rect.width, rect.height - y);
        ctx.fill();

        const v = surface.scale * 100;
        ctx.beginPath();
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'center';
        ctx.font = '12px sans-serif';
        ctx.fillStyle = v > 286 ? "#333333aa" : "#ffffffaa";
        ctx.fillText(`${v.toFixed(0)}%`, rect.width/2, rect.height/2);
    }

    _computeValue(y: number) {
        const h = this.c.getBoundingClientRect().height;
        const percent = (h - y) / h;
        return SurfaceConst.ZOOM_MIN + percent * (SurfaceConst.ZOOM_MAX - SurfaceConst.ZOOM_MIN)
    }

    _computeY(scale: number) {
        const p = (scale - SurfaceConst.ZOOM_MIN) / (SurfaceConst.ZOOM_MAX - SurfaceConst.ZOOM_MIN);
        const h = this.c.getBoundingClientRect().height;
        return h - p * h;
    }

    /** MOUSE HANDLING */
    mouseDown: boolean = false;
    mouseY: number = 0;

    _getXY(evt: MouseEvent) {
        const rect = this.c.getBoundingClientRect();
        return [evt.pageX - rect.x, evt.pageY - rect.y];
    }

    onDown(evt:MouseEvent) {
        this.mouseDown = true;
        const [, y] = this._getXY(evt);
        this.mouseY = y;
    }

    onMove(evt:MouseEvent) {
        if (this.mouseDown) {
            const [, y] = this._getXY(evt);
            const y0 = this._computeValue(this.mouseY);
            const y1 = this._computeValue(y);
            const dy = y1 - y0;
            this.onZoomSliderUpdate(surface.scale + dy);
            this.mouseY = y;
        }

        this.draw();
    }

    onUp(evt:MouseEvent) {
        this.mouseDown = false;
    }

    updateSize() {
        const rect = this.c.getBoundingClientRect();
        this.c.width = rect.width;
        this.c.height = rect.height;

        this.draw();
    }
}

export const zoomSlider = new ZoomSlider();