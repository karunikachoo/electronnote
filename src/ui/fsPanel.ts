import {SurfaceConst} from "../canvas/surfaceConst";
import {TokenComponents} from "../objects/text/tokenComponents";
import {KeyboardListener, stateMachine} from "../logic/stateMachine";
import {ipcRenderer, IpcRendererEvent} from "electron";
import {db} from "../db/db";
import {sanitize} from "../tools/sanitise";

export class FItem {
    fname: string;
    nid: string;
    open: boolean;
}


class FSPanel implements KeyboardListener {
    static readonly VAL = 80;
    static readonly OFFSET = 50;

    w: number = 80;
    h: number = 1000;
    ox: number = 0;
    oy: number = FSPanel.OFFSET;
    c: HTMLCanvasElement;

    shown: boolean = false;

    items: FItem[] = [];
    filteredItems: FItem[] = [];
    search: string = "";

    fontsize: number = 28;
    lineHeight: number = 2.5;
    itemHeight: number = 0;

    select:number = -1;

    onClickCallback: (id: string) => void;

    init() {
        this.c = document.getElementById('fs') as HTMLCanvasElement;
        this.updateSize = this.updateSize.bind(this);

        this.onMouseMove = this.onMouseMove.bind(this);
        this.onDblClick = this.onDblClick.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onWheel = this.onWheel.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);

        this.onFilePath = this.onFilePath.bind(this);

        this.c.onmousemove = this.onMouseMove;
        this.c.ondblclick = this.onDblClick;
        this.c.onclick = this.onClick;
        this.c.onwheel = this.onWheel;
        this.c.onkeypress = this.onKeyPress;
        this.c.onkeydown = this.onKeyDown;

        this.c.addEventListener('transitionend', this.updateSize);
        ipcRenderer.on('fs-dialog-ret', this.onFilePath);

        this.updateSize();
        this._filterItems();
    }

    setItems(items: FItem[], show: boolean) {
        this.items = items;
        this._filterItems();
        if (!this.shown && show) this.show();
        else this.draw();
    }

    addItem(item: FItem, show: boolean) {
        this.items.push(item);
        this._filterItems();
        if (!this.shown && show) this.show();
        else this.draw();
    }

    _filterItems() {
        if (this.search == "") {
            this.filteredItems = this.items;
        } else {
            this.filteredItems = this.items.filter(item => {
                return item.fname.toLowerCase().includes(this.search);
            })
        }
    }

    onFilePath(evt: IpcRendererEvent, filePaths: string[]) {
        if (filePaths.length > 0) {
            db.loadDirectory(filePaths[0]);
        }
    }

    onWheel(evt: WheelEvent) {
        if (this.itemHeight > this.c.height) {
            this.oy -= evt.deltaY;
            const hx = this.c.height - this.itemHeight;
            if (this.oy < hx - 5) {
                this.oy = hx - 5;
            } else if (this.oy > FSPanel.OFFSET) {
                this.oy = FSPanel.OFFSET;
            }
            this.draw();
        }
    }

    _checkMause(x:number, y:number) {
        const height = this.fontsize * this.lineHeight;
        const n = Math.floor((y - FSPanel.VAL/6 - this.oy) / height);
        if (n < this.filteredItems.length) {
            return n
        } else {
            return -1;
        }
    }

    onDblClick(evt: MouseEvent) {
        const [x, y] = [
            evt.pageX * SurfaceConst.FIDEL_MULT,
            evt.pageY * SurfaceConst.FIDEL_MULT];
        const x0 = this.w + this.ox - FSPanel.VAL;

        if (x >= 0 && x <= x0 && y >= FSPanel.OFFSET) {
            const n = this.select = this._checkMause(x, y);
            if (n >= 0 && n < this.filteredItems.length) {
                const id = this.filteredItems[n].nid;
                if (this.onClickCallback) {
                    this.onClickCallback(id)
                    this.onClickCallback = null;
                } else {
                    db.openNote(id);
                }
                this.hide();
            }
        }
    }

    onClick(evt: MouseEvent) {
        const [x, y] = [
            evt.pageX * SurfaceConst.FIDEL_MULT,
            evt.pageY * SurfaceConst.FIDEL_MULT];

        const [x0, y0, x1, y1] = [
            this.w + this.ox - FSPanel.VAL, 0,
            this.w + this.ox, FSPanel.VAL
        ];

        if (x >= x0 && x <= x1 && y >= y0 && y <= y1) {
            if (this.shown) this.hide()
            else this.show();

        } else if (x >= x0 && x <= x1 && y >= y0 + FSPanel.VAL && y < y1 + FSPanel.VAL) {
            if (!this.shown) this.show();
            else ipcRenderer.send('fs-dialog-req');
        }
        // } else if (x >= 0 && x <= x0 && y >= FSPanel.OFFSET && this.shown) {
        //     const height = this.fontsize * this.lineHeight;
        //     const n = Math.floor((y - this.oy) / height);
        //     if (n < this.filteredItems.length) {
        //         this.select = n;
        //     } else {
        //         this.select = -1;
        //     }
        //     this.draw();
        // } else {
        //     this.select = -1;
        //     this.draw();
        // }
    }

    onMouseMove(evt: MouseEvent) {
        const [x, y] = [
            evt.pageX * SurfaceConst.FIDEL_MULT,
            evt.pageY * SurfaceConst.FIDEL_MULT];

        const [x0, y0, x1, y1] = [
            this.w + this.ox - FSPanel.VAL, 0,
            this.w + this.ox, FSPanel.VAL
        ];

        if (x >= 0 && x <= x0 && y >= FSPanel.OFFSET && this.shown) {
            this.select = this._checkMause(x, y)
            this.draw();
        } else {
            this.select = -1;
            this.draw();
        }
    }

    onKeyPress(evt: KeyboardEvent) {
        if (evt.key != 'Enter') this.search += evt.key
        this._filterItems();
        this.draw();
    }

    onKeyDown(evt: KeyboardEvent) {
        if (evt.key == 'Escape') {
            if (this.search != '') {
                this.search = '';
            } else if (this.onClickCallback) {
                this.onClickCallback(null);
                this.onClickCallback = null;
                this.hide();
            }
        } else if (evt.key == 'Backspace') {
            this.search = this.search.slice(0, this.search.length - 1);
        }
        this._filterItems();
        this.draw()
    }

    draw() {
        const ctx = this.c.getContext('2d');
        ctx.clearRect(0, 0, this.c.width, this.c.height);

        this._drawItems(ctx);
        this._drawSearchTerm(ctx);
        this._drawIcon(ctx, FSPanel.VAL);
        this._drawOpenDirIcon(ctx, FSPanel.VAL);
    }

    _drawOpenDirIcon(ctx: CanvasRenderingContext2D, v:number) {
        ctx.strokeStyle = "#ffffff55";
        ctx.lineWidth = 4;
        ctx.lineCap = 'round';

        const oy = v;

        ctx.beginPath();
        ctx.moveTo(this.w - 0.75 * v, oy + 0.35 * v);
        ctx.lineTo(this.w - 0.60 * v, oy + 0.35 * v);
        ctx.lineTo(this.w - 0.50 * v, oy + 0.425 * v);
        ctx.lineTo(this.w - 0.35 * v, oy + 0.425 * v);
        ctx.lineTo(this.w - 0.35 * v, oy + 0.65 * v);
        ctx.lineTo(this.w - 0.75 * v, oy + 0.65 * v);
        ctx.lineTo(this.w - 0.75 * v, oy + 0.35 * v);

        ctx.lineTo(this.w - 0.75 * v, oy + 0.25 * v);
        ctx.lineTo(this.w - 0.60 * v, oy + 0.25 * v);
        ctx.lineTo(this.w - 0.50 * v, oy + 0.325 * v);
        ctx.lineTo(this.w - 0.35 * v, oy + 0.325 * v);
        ctx.lineTo(this.w - 0.35 * v, oy + 0.55 * v);

        ctx.stroke();
    }

    _drawIcon(ctx: CanvasRenderingContext2D, v:number) {
        if (this.shown) {
            ctx.strokeStyle = "#ffffff55";
            ctx.lineWidth = 4;
            ctx.lineCap = 'round';
            ctx.beginPath();
            ctx.moveTo(this.w - 0.35 * v , 0.35 * v);
            ctx.lineTo(this.w - 0.6 * v, 0.5 * v);
            ctx.lineTo(this.w - 0.35 * v, 0.65 * v);
            ctx.stroke();
        } else {
            ctx.strokeStyle = "#ffffff55";
            ctx.lineWidth = 4;
            ctx.lineCap = 'round';
            ctx.beginPath();
            ctx.moveTo(this.w - 0.65 * v, 0.35 * v);
            ctx.lineTo(this.w - 0.4 * v, 0.5 * v);
            ctx.lineTo(this.w - 0.65 * v, 0.65 * v);
            ctx.stroke();
        }
    }

    _drawSearchTerm(ctx: CanvasRenderingContext2D) {
        ctx.fillStyle = "#222";
        ctx.textBaseline = 'middle';
        ctx.font = `normal 200 ${this.fontsize}px monospace`;
        const h = this.lineHeight * this.fontsize * 0.9;
        ctx.fillRect(0, 0, this.w, h)
        ctx.fillStyle = "#fff";
        ctx.fillText(this.search, FSPanel.VAL/3, h/1.75)
    }

    _drawItems(ctx: CanvasRenderingContext2D) {
        const height = this.fontsize * this.lineHeight;
        const w = this.w - FSPanel.VAL;
        let off = FSPanel.VAL/6;
        ctx.textBaseline = 'middle';
        ctx.font = `normal 200 ${this.fontsize}px sans-serif`;
        ctx.lineWidth = 1;

        for(let i = 0; i < this.filteredItems.length; i++) {
            let item = this.filteredItems[i].fname;
            ctx.fillStyle = i == this.select ? "#ffffff22" : "#ffffff08";
            TokenComponents._pathRoundRect(
                ctx, 15, this.oy + off,
                w -16, height - 10, 5, 1);
            ctx.fill();

            let ww = ctx.measureText(item).width;
            if (ww > w - 60) {
                while (ww > w - 80) {
                    item = item.slice(0, item.length - 1);
                    ww = ctx.measureText(item).width;
                }
                item += "...";
            }

            ctx.beginPath();
            ctx.fillStyle = "#fff";
            ctx.fillText(item, FSPanel.VAL/2.2, this.oy + off + height/2);

            if (this.filteredItems[i].open) {
                ctx.beginPath();
                ctx.arc(w - 35, this.oy + off + height/2.25, 7.5, 0, 2 * Math.PI);
                ctx.fill();
            }

            off += height;
        }

        this.itemHeight = off;
    }

    hide() {
        this.c.style.transform = 'translateX(-17.5rem)';
        this.shown = false;
        stateMachine.removeKeyboardLister(this);
    }

    show() {
        this.c.style.transform = 'translateX(0rem)';
        this.shown = true;
        stateMachine.addKeyboardListener(this);
    }

    updateSize() {
        const rect = this.c.getBoundingClientRect();
        this.c.width = rect.width * SurfaceConst.FIDEL_MULT;
        this.c.height = rect.height * SurfaceConst.FIDEL_MULT;
        this.w = this.c.width;
        this.h = this.c.height;
        this.ox = (rect.x) * SurfaceConst.FIDEL_MULT;

        this.draw();
    }

}

export const fsPanel = new FSPanel();