export type DiagCall = (confirm: boolean) => void;

export class DialogBox {

    wrapper: HTMLElement;
    dialog: HTMLElement;

    title: HTMLElement;
    msg: HTMLElement;

    confirm: HTMLButtonElement;
    cancel: HTMLButtonElement;

    eventCallback: DiagCall;

    init() {
        this.wrapper = document.getElementById('dialog-overlay');
        this.dialog = document.getElementById('dialog');
        this.title = document.getElementById('dialog-title');
        this.msg = document.getElementById('dialog-msg');
        this.confirm = document.getElementById('dialog-confirm') as HTMLButtonElement;
        this.cancel = document.getElementById('dialog-cancel') as HTMLButtonElement;

        this.hide()

        this.cancel.onclick = () => {
            this.hide();
            if (this.eventCallback) this.eventCallback(false);
        }

        this.confirm.onclick = () => {
            this.hide();
            if (this.eventCallback) this.eventCallback(true);
        }
    }

    setText(title: string, msg: string) {
        this.title.innerText = title;
        this.msg.innerText = msg;
    }

    hide() {
        this.wrapper.style.display = 'none';
    }

    show(callback?: DiagCall) {
        this.eventCallback = callback;

        this.wrapper.style.opacity = '0';
        this.dialog.style.opacity = '0';
        this.wrapper.style.display = 'flex';
        this.wrapper.style.opacity = '1';
        this.dialog.style.opacity = '1';
    }

}

export const dialogBox = new DialogBox();