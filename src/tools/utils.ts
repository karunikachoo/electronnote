
const alphaNum = "abcdefghijklmnopqrstuvwxyz0123456789"
export class Utils {
    static genId(n: number) {
        let ret = "";
        for (let i = 0; i < n; i++) {
            ret += alphaNum[Math.floor(Math.random() * alphaNum.length)];
        }
        return ret;
    }

    static hash(str: string) {
        let hash = 5381,
            i    = str.length;

        while(i) {
            hash = (hash * 33) ^ str.charCodeAt(--i);
        }

        /* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
         * integers. Since we want the results to be always positive, convert the
         * signed int to an unsigned by doing an unsigned bitshift. */
        return hash >>> 0;
    }

    static dirtySanitise(text: string) {
        return text.replace(/[^a-z0-9]/gi, '_');
    }

    static lerp(fr:number, to:number, dt:number) {
        if (dt > 1) dt = 1;
        if (dt < 0) dt = 0;
        return fr + (to-fr) * dt;
    }

    static _arrayBufferToBase64( buffer: ArrayBuffer ) {
        let binary = '';
        let bytes = new Uint8Array( buffer );
        let len = bytes.byteLength;
        for (let i = 0; i < len; i++) {
            binary += String.fromCharCode( bytes[ i ] );
        }
        return window.btoa( binary );
    }

    static shallowEqual(object1: Object, object2: Object) {
        const keys1 = Object.keys(object1);
        const keys2 = Object.keys(object2);

        if (keys1.length !== keys2.length) {
            return false;
        }

        for (let key of keys1) {
            // @ts-ignore
            // console.log(object1[key], object2[key])
            // @ts-ignore
            if (object1[key] !== object2[key]) {
                return false;
            }
        }

        return true;
    }
}