

export class Colors {
    static readonly COLORS = [
        "#5062db",
        "#58A4B0",
        "#d75a43",
        "#8DB580",
        "#F45866",
        "#68A691",
        "#FE5E41",
        "#de8d16",
        "#FE5F55"
    ]

    static random() {
        return Colors.COLORS[Math.floor(Math.random() * Colors.COLORS.length)];
    }
}