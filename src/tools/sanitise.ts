const illegalRe = /[\/\?<>\\:\*\|"]/g;
const controlRe = /[\x00-\x1f\x80-\x9f]/g;
const reservedRe = /^\.+$/;
const windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
const windowsTrailingRe = /[\. ]+$/;

function _sanitize(input:string, replacement:string) {
    if (typeof input !== 'string') {
        throw new Error('Input must be string');
    }
    return input
        .replace(illegalRe, replacement)
        .replace(controlRe, replacement)
        .replace(reservedRe, replacement)
        .replace(windowsReservedRe, replacement)
        .replace(windowsTrailingRe, replacement);
}

export function sanitize(input:string, replacement?: string) {
    let output = _sanitize(input, replacement || '');
    if (replacement === '') {
        return output;
    }
    return _sanitize(output, '');
};