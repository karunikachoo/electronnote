

class Title {
    id: string = "";
    span_id: string = "";

    init(id: string, span_id: string) {
        this.id = id;
        this.span_id = span_id;

        const title: HTMLInputElement = document.getElementById('title') as HTMLInputElement;
        const title_width = document.getElementById('title-span');

        title.onkeydown = (evt) => {
            title_width.classList.remove('hide');
            title_width.innerText = title.value;
            title.style.width = title_width.scrollWidth + 'px';
            title_width.classList.add('hide');
        }
    }
}

export const title = new Title();