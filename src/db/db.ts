import {Note} from "../objects/note";
import * as fs from "fs";
import {FItem, fsPanel} from "../ui/fsPanel";
import {surface} from "../canvas/surface";
import {homedir} from "os";
import {Utils} from "../tools/utils";
import {sanitize} from "../tools/sanitise";


export class DBConst {
    static readonly FTYPE = '.json';
}

export class DBNote {
    filepath: string;
    id: string;
    title: string;
    rect: number[];
    sections: string[];
    objects: Object[];

    inherit(n: DBNote) {
        this.filepath = n.filepath;
        this.id = n.id;
        this.title = n.title;
        this.rect = n.rect;
        this.sections = n.sections;
        this.objects = n.objects;

        return this;
    }

    fromNote(note: Note, force:boolean=false) {
        if (this.id == undefined) {
            this.id = note.id;
        } else if (!force && this.id && this.id != note.id) {
            console.error(`fromNote: Incorrect Note ID -> ${this.id} != ${note.id}`);
            return this;
        } else if (force) {
            this.id = note.id;
        }
        this.rect = note.rect;
        this.title = note.title.text;
        this.sections = note.getSections();
        this.objects = note.getJSONObjects();
        return this;
    }

    hasChanged(note: Note) {
        let change = false;
        if (note) {
            if (this.title != note.title.text) {
                console.log("Δ Title")
                change = true
            } else if (this.hasSectionChanged(note.getSections())) {
                console.log("Δ Section")
                change = true;
            } else if (this.rect[2] != note.rect[2]) {
                console.log("Δ Width")
                change = true;
            } else if (this.hasObjectsChanged(note.getJSONObjects())) {
                console.log("Δ objects")
                change = true;
            }
        }

        return change;
    }

    hasObjectsChanged(objs: Object[]): boolean {
        let ret = false;
        if (this.objects.length != objs.length) {
            ret = true;
        } else {
            for (let i = 0; i < objs.length; i++) {
                if (!Utils.shallowEqual(this.objects[i], objs[i])) {
                    ret = true;
                }
            }
        }
        return ret;
    }

    hasSectionChanged(sections: string[]): boolean {
        if (this.sections.length != sections.length) {
            return true
        } else {
            for (let i = 0; i < sections.length; i++) {
                if (this.sections[i] != sections[i]) {
                    return true;
                }
            }
            return false;
        }
    }

    toNote() {
        const [x, y, w, h] = this.rect;
        const n = new Note(surface, x, y, w, h, this.sections, this.id);
        n.title.text = this.title;
        n.parseObjects(this.objects);
        return n;
    }

    toJSON() {
        return JSON.stringify({
            id: this.id,
            title: this.title,
            rect: this.rect,
            sections: this.sections,
            objects: this.objects
        });
    }
}


class Database {
    notes: Map<string, DBNote> = new Map<string, DBNote>();
    changes: Map<string, boolean> = new Map<string, boolean>();

    homeDir: string = homedir();

    config = {
        lastWorkspacePath: "",
        lastFolder: ""
    }

    configHash: number = 0;

    lastUpdate: number = 0;

    init() {
        this._loadConfig();
    }

    dbSaveCheck() {
         const now = Date.now();
         if (now - this.lastUpdate > 10000 && this.configHash != 0) {
             // console.log("scheduled saving");
             this._saveConfig();
             // this.saveNotes();
             // this.reloadDirectory();
             this.lastUpdate = now;
         }
    }

    /**
     * NOTE HANDLING
     * */

    getNote(id: string) {
        return this.notes.get(id);
    }

    hasNote(id: string) {
        return this.notes.has(id);
    }

    openNote(id: string) {
        let n: Note;
        if (this.notes.has(id)) {

            if (!surface.has(id)) {
                const dbn = this.notes.get(id);
                n = dbn.toNote();
                surface.addObject(n);
            } else {
                // TODO: Jump to note. Animate?;
                n = surface.getObject(id) as Note;
            }
            surface.scrollTo(n);
        }
        return n;
    }

    updateNote(note: Note) {
        if (!this.notes.has(note.id)) {
            this.notes.set(note.id, new DBNote().fromNote(note));
            this.changes.set(note.id, true);
            return;
        }

        const ndb =  this.notes.get(note.id);
        let change = ndb.hasChanged(note);
        if (change && !this.changes.get(note.id)) {
            this.changes.set(ndb.id, change);
        }
        ndb.fromNote(note);
        return this.changes.get(ndb.id);
    }

    saveNote(id: string) {
        this.updateNote(surface.getObject(id) as Note);
        if (this.changes.get(id)) {
            const n = this.notes.get(id);

            if (n.title != undefined && n.title != "") {
                if (n.filepath == undefined) {
                    n.filepath = this.config.lastFolder + `/${sanitize(n.title)}.${n.id}${DBConst.FTYPE}`;
                }

                const data = n.toJSON();
                fs.writeFile(n.filepath, data, () => {
                    console.log(`${n.id} saved!`);
                });
                this.changes.set(n.id, false);
            }
            this.reloadDirectory();
        }
    }

    isNoteSaved(id: string) {
        if (this.changes.has(id))
            return !this.changes.get(id);
        else return false;
    }

    deleteNote(id: string) {
        if (this.notes.has(id)) {
            const dbn = this.notes.get(id);
            this.notes.delete(id);
            this.changes.delete(id);

            if (fs.existsSync(dbn.filepath)) {

                fs.unlink(dbn.filepath, err => {
                    if (!err) {
                        this.reloadDirectory();
                    } else {
                        console.error(err);
                    }
                });
            }
        }
    }

    saveNotes() {
        this.notes.forEach((n, id) => {
            this.saveNote(id);
        })
    }

    /**
     * FS PANEL DIRECTORY LOADING HANDLING
     * */

    loadDirectory(path: string) {
        this.notes.clear();
        this.changes.clear();

        this._readdir(path, true);
        this.config.lastFolder = path;
    }

    reloadDirectory(reset=false) {
        if (reset) {
            this.notes.clear();
            this.changes.clear();
        }
        this._readdir(this.config.lastFolder, false);
    }

    _readdir(path: string, show: boolean) {
        fs.readdir(path, (err, files) => {
            if (!err) {
                const notePaths: string[] = [];

                files.forEach(fname => {
                    if (fname.includes(DBConst.FTYPE)) {
                        notePaths.push(fname);
                    } else {
                        const fpath = `${path}/${fname}`;
                        this._readdir(fpath, show);
                    }
                })

                notePaths.forEach(file => {
                    const fpath = `${path}/${file}`;

                    const data = fs.readFileSync(fpath);
                    const str = data.toString('utf-8');
                    try {
                        const n = new DBNote().inherit(JSON.parse(str) as DBNote);
                        n.filepath = fpath;
                        this.notes.set(n.id, n);
                        this.changes.set(n.id, false);
                    } catch (e) {
                        console.error(e, str);
                    }
                })
                this._updateFSPanel(show);
            }
        });
    }

    _updateFSPanel(show: boolean) {
        const fsitems: FItem[] = [];
        this.notes.forEach(n => {
            const i = new FItem();
            i.fname = n.title;
            i.nid = n.id;
            i.open = surface.has(n.id);
            fsitems.push(i);
        })

        fsitems.sort((a, b) => {
            return a.fname > b.fname ? 1 : -1;
        })

        fsPanel.setItems(fsitems, show);
    }

    /**
     * CONFIG HANDLING
     * */

    _loadConfig() {
        const configPath = this.homeDir + '/.electronNote';
        fs.readFile(configPath, (err, f) => {
            if (err) {
                fs.writeFile(configPath, JSON.stringify(this.config), () => {
                    console.log("Config Saved");
                })
            } else {
                const data = f.toString('utf-8');
                this.config = JSON.parse(data);
                this.configHash = Utils.hash(data);

                console.log(this.config);

                if (this.config.lastFolder != "") {
                    this.loadDirectory(this.config.lastFolder);
                }
            }
        })
    }

    _saveConfig() {
        const configPath = this.homeDir + '/.electronNote';
        const data = JSON.stringify(this.config);
        const hash = Utils.hash(data);
        if (this.configHash != hash) {
            fs.writeFile(configPath, data, () => {
                console.log("Config Saved")
            });
            this.configHash = hash;
        }
    }

}

export const db = new Database();