import {SurfaceObject} from "./surfaceObject";
import {KeyboardListener, stateMachine} from "../logic/stateMachine";


export class SurfaceObjectKB extends SurfaceObject implements KeyboardListener {

    _activate() {
        super._activate();
        stateMachine.addKeyboardListener(this);
    }

    _deactivate() {
        super._deactivate();
        stateMachine.removeKeyboardLister(this);
    }

    onKeyDown(e: KeyboardEvent): void {
    }

    onKeyPress(e: KeyboardEvent): void {
    }

}