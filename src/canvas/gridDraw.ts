

export class GridDraw {
    div: number;
    strokeColor: string;

    constructor(div: number = 100, stroke: string = '#cccccc11') {
        this.div = div;
        this.strokeColor = stroke;
    }

    draw(ctx: CanvasRenderingContext2D, x: number, y: number, w: number, h: number, s: number) {
        ctx.strokeStyle = this.strokeColor;

        const k = this.div;
        const n = [Math.ceil(w / k / s), Math.ceil(h / k / s)];
        const d = [x % k, y % k];

        // cols
        for (let i = 0; i <= n[0]; i++) {
            const x = (i * k + d[0]) * s;
            this._line(ctx, [x, 0], [x, h]);
        }

        // rows
        for (let i = 0; i <= n[1]; i++) {
            const y = (i * k + d[1]) * s;
            this._line(ctx, [0, y], [w, y]);
        }
    }

    _line(ctx: CanvasRenderingContext2D, xy0: number[], xy1: number[]) {
        ctx.beginPath();
        ctx.moveTo(xy0[0], xy0[1]);
        ctx.lineTo(xy1[0], xy1[1]);
        ctx.stroke();
    }
}