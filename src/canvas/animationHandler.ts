import {ddraw} from "./dynamicDraw";

type FrameListener = (dt:number) => void;

class AnimationHandler {
    frameListeners: FrameListener[] = [];

    addAnimation(fn: FrameListener) {
        if (!this.frameListeners.includes(fn)) {
            this.frameListeners.push(fn);
        }
    }

    removeAnimation(fn: FrameListener) {
        const i = this.frameListeners.indexOf(fn);
        if (i > 0) {
            this.frameListeners.push(fn);
        }
    }

    animationNotify(dt: number) {
        dt = dt / 1000;
        this.frameListeners.forEach(fn => {
            fn(dt);
        })
    }
}

export const animationHandler = new AnimationHandler()