import {SurfaceObject} from "./surfaceObject";

export class SurfaceObjectResizeHelper {
    static readonly TAB_SIZE = 25;
    parent: SurfaceObject;

    constructor(parent: SurfaceObject) {
        this.parent = parent;
    }

    mouseCheck(ox:number, oy:number, mx:number, my:number, s:number): boolean {
        const v = SurfaceObjectResizeHelper.TAB_SIZE;
        const [x1, y1] = [
            (ox + this.parent.rect[2]) * s,
            (oy + this.parent.rect[3]) * s
        ];
        const [x0, y0] = [
            x1 - (2.5 * v) * s,
            y1 - (2.5 * v) * s
        ];

        return (mx >= x0 && mx <= x1) && (my >= y0 && my <= y1);
    }

    draw(ctx: CanvasRenderingContext2D,
         x: number, y: number, s: number) {

        const v = SurfaceObjectResizeHelper.TAB_SIZE;
        const [x1, y1] = [x+this.parent.rect[2]-v/2, y+this.parent.rect[3]-v/2];

        ctx.beginPath();
        ctx.moveTo((x1 - v) * s, y1 * s);
        ctx.lineTo(x1 * s, (y1 - v) * s);   // /
        // ctx.arcTo(x1 * s, y1 * s, (x1 - v) * s, y1 * s, Note.RADIUS - v/2 );
        ctx.lineTo(x1 * s, y1 * s);         //  |
        ctx.lineTo((x1 - v) * s, y1 * s)    // _

        ctx.fillStyle = '#22222288';
        ctx.fill();
    }
}