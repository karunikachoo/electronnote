import {surface} from "./surface";

export class Cursor {
    static readonly DEFAULT = 'default';
    static readonly TEXT = 'text';
    static readonly CROSSHAIR = 'crosshair';
    static readonly POINTER = 'pointer';
    static readonly SE_RESIZE = 'se-resize';
    static readonly GRABBING = 'grabbing';

    static set(c: string) {
        surface.c.style.cursor = c;
    }

    static reset() {
        surface.c.style.cursor = Cursor.DEFAULT;
    }
}