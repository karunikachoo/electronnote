import {SurfaceObject} from "./surfaceObject";
import {GridDraw} from "./gridDraw";
import {ddraw} from "./dynamicDraw";
import {NumUtils} from "../tools/numberUtils";
import {SurfaceConst} from "./surfaceConst";
import {fsPanel} from "../ui/fsPanel";
import {SurfaceObjectKB} from "./surfaceObjectKB";
import {db} from "../db/db";
import {linkHandler} from "../objects/linkHandler";
import {Note} from "../objects/note";
import {Utils} from "../tools/utils";
import {animationHandler} from "./animationHandler";
import {zoomSlider} from "../ui/zoomSlider";
import {MdCodeBlockObj} from "../objects/text/mdCodeBlock";


export class Surface extends SurfaceObjectKB{
    id = "Main"

    infinite = true;

    c: HTMLCanvasElement;
    scale: number = 1;

    objectMap: Map<string, SurfaceObject> = new Map()

    grid: GridDraw = new GridDraw(50);

    drawCount = 0;
    hoverItem: SurfaceObject | MdCodeBlockObj = this;

    init(id: string) {
        this.c = document.getElementById(id) as HTMLCanvasElement;

        this.onPan = this.onPan.bind(this);
        this.onZoom = this.onZoom.bind(this);
        this.updateSize = this.updateSize.bind(this);
        this._scrollCheck = this._scrollCheck.bind(this);

        animationHandler.addAnimation(this._scrollCheck)

        this.updateSize();
    }


    has(id: string) {
        // for(let i = 0; i < this.objects.length; i++) {
        //     if (this.objects[i].id == id) return true;
        // }
        return this.objectMap.has(id);
    }

    addObject(obj: SurfaceObject) {
        super.addObject(obj);
        this.objectMap.set(obj.id, obj);
        db._updateFSPanel(false);
    }

    removeObject(obj: SurfaceObject) {
        if (this.objectMap.has(obj.id)) this.objectMap.delete(obj.id);
        super.removeObject(obj);
        db._updateFSPanel(false);
    }

    getObject(id: string) {
        // for(let i = 0; i < this.objects.length; i++) {
        //     if (this.objects[i].id == id) return this.objects[i];
        // }
        if (this.objectMap.has(id)) return this.objectMap.get(id);
        return null;
    }

    clear() {
        this.objectMap.clear();
        this.objects = [];
        ddraw.event();
    }

    getOffset() { return this.rect; }

    updateHover(item: SurfaceObject | MdCodeBlockObj) {
        this.hoverItem.pannable = false;
        this.hoverItem = item;
        this.hoverItem.pannable = true;
    }

    setScale(value: number, x:number, y:number) {
        ddraw.event();
        const oldScale = this.scale;
        this.scale = NumUtils.clamp(value, SurfaceConst.ZOOM_MIN, SurfaceConst.ZOOM_MAX);

        this.rect[0] += (x / this.scale) - (x / oldScale);
        this.rect[1] += (y / this.scale) - (y / oldScale);
        this.targetOffset[0] = this.rect[0];
        this.targetOffset[1] = this.rect[1];
    }

    _draw() {
        if (!this.c) return;

        // this.drawCount++;
        // invalidate screen;
        const ctx = this.c.getContext('2d');
        ctx.clearRect(0, 0, this.c.width, this.c.height);

        this.grid.draw(ctx, this.rect[0], this.rect[1], this.c.width, this.c.height, this.scale);
        this._forceFontUpdate(ctx);

        // check if surfaceObject within screen
        // let draw: SurfaceObject[] = [];
        this._filterAndDrawObjects(ctx, this.rect[0], this.rect[1], this.scale);

        linkHandler.drawLinks(ctx, this.rect[0], this.rect[1], this.scale);
    }

    _filterAndDrawObjects(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number) {
        for (let k = 0; k < this.objects.length; k++) {
            const o = this.objects[k];
            if (o.rect[2] > 0 && o.rect[3] > 0) {
                const [x, y, w, h] = o.rect;
                const cx = [
                    (ox + x) * s,
                    (ox + x + w) * s,
                ];

                const cy = [
                    (oy + y) * s,
                    (oy + y + h) * s
                ];

                let draw = false;

                if (this._inCanvas(cx[0], cy[0])
                    || this._inCanvas(cx[1], cy[0])
                    || this._inCanvas(cx[1], cy[1])
                    || this._inCanvas(cx[0], cy[1])) {
                    draw = true;
                    // console.log("draw");
                } else if (cy[0] < 0 && cy[1] > this.c.height &&
                    cx[0] < 0 && cx[1] > this.c.width) {
                    draw = true;
                    // console.log("span xy");
                } else if (cx[0] < 0 &&  cx[1] > this.c.width) {
                    if (this._inCanvas(0, cy[0]) ||
                        this._inCanvas(0, cy[1])) {
                        draw = true;
                        // console.log("span x")
                    }
                } else if (cy[0] < 0 && cy[1] > this.c.height) {
                    if (this._inCanvas(cx[0], 0) ||
                        this._inCanvas(cx[1], 0)) {
                        draw = true;
                        // console.log("span y")
                    }
                }

                if (draw) o.draw(ctx, ox, oy, s);
            }
        }
    }

    _inCanvas(x:number, y:number) {
        return x >= 0 && x < this.c.width && y >= 0 && y <= this.c.height;
    }

    _forceFontUpdate(ctx: CanvasRenderingContext2D) {
        ctx.fillStyle = '#fff';
        ctx.font = `normal 300 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.fillStyle = '#fff';
        ctx.font = `normal normal 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.fillStyle = '#fff';
        ctx.font = `normal bold 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.fillStyle = '#fff';
        ctx.font = `italic 300 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.fillStyle = '#fff';
        ctx.font = `italic normal 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.fillStyle = '#fff';
        ctx.font = `italic bold 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.fillStyle = '#fff';
        ctx.font = `normal 300 24px RobotoMono`;
        ctx.fillText('0', -50, -50);
        // ctx.fillText(
        //     `${x.toFixed(2)} : ${y.toFixed(2)}`,
        //     x * s,y * s);
    }

    /**
     * STATE CLICKS
     */

    onStateClick(mId:number, evt:MouseEvent) {
        fsPanel.hide();
        this.onClick(mId, 0, 0, this.scale, evt);
    }

    onStateDoubleClick(mId:number, evt:MouseEvent) {
        this.onDoubleClick(mId, 0, 0, this.scale, evt);
    }

    onStateMouseUp(mId:number, evt:MouseEvent) {
        this.onMouseUp(mId, 0, 0, this.scale, evt);
    }

    onStateMouseMove(dxy: number[], evt: MouseEvent) {
        this.onMouseMove(dxy, 0, 0, this.scale, evt);
    }

    onStateDrag(dxy:number[], evt:MouseEvent) {
        this.onDrag(dxy, 0, 0, this.scale, evt);
    }

    /**
     * ACTUAL CLICKS
     */

    onClick(mId: number, ox: number, oy: number, s:number, evt: MouseEvent) {
        ddraw.event();
        if (!linkHandler.onClick(mId, ox, oy, s, evt))
            super.onClick(mId, ox, oy, s, evt);
    }

    onDoubleClick(mId: number, ox: number, oy: number, s: number, evt: MouseEvent) {
        ddraw.event();
        super.onDoubleClick(mId, ox, oy, s, evt);
    }

    onMouseUp(mId: number, ox: number, oy: number, s: number, evt: MouseEvent) {
        ddraw.event();
        super.onMouseUp(mId, ox, oy, s, evt);
    }

    onMouseMove(dxy: number[], ox: number, oy: number, s: number, evt: MouseEvent) {
        if (!linkHandler.onHover(ox, oy, s, evt))
            super.onMouseMove(dxy, ox, oy, s, evt);
    }

    onDrag(dxy: number[], ox: number, oy: number, s:number, evt: MouseEvent) {
        ddraw.event();
        super.onDrag(dxy, ox, oy, s, evt);
    }

    onZoom(dy: number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        this.setScale(this.scale + dy, x, y);
        zoomSlider.draw();
    }

    onPan(dxy: number[], evt: MouseEvent) {
        ddraw.event();
        if (!this.onScroll(dxy, 0, 0, this.scale, evt)) {
            this.rect[0] += dxy[0] * SurfaceConst.FIDEL_MULT;
            this.rect[1] += dxy[1] * SurfaceConst.FIDEL_MULT;
            this.onObjectMove(this.rect);
        }
    }

    onObjectMove(rect: number[]) {
        this.targetOffset[0] = rect[0];
        this.targetOffset[1] = rect[1];
    }

    updateSize() {
        ddraw.event();
        const rect = this.c.getBoundingClientRect();
        this.c.width = rect.width * SurfaceConst.FIDEL_MULT;
        this.c.height = rect.height * SurfaceConst.FIDEL_MULT;
    }

    onKeyDown(e: KeyboardEvent) {
        if ((e.metaKey || e.ctrlKey) && e.key == 's') {
            db.saveNotes();
        }
    }

    /**
     * Animation frame request
     */
    targetOffset = [0, 0];
    scrollSpeed = 5;

    scrollTo(o: SurfaceObject) {
        this.targetOffset[0] = (this.c.width / 2) / this.scale - (o.rect[0] + o.rect[2]/2);
        this.targetOffset[1] = (this.c.height / 6) / this.scale - o.rect[1];
    }

    _scrollCheck(dt:number) {
        if (Math.abs(this.targetOffset[0] - this.rect[0]) > 0.005 ||
            Math.abs(this.targetOffset[1] - this.rect[1]) > 0.005) {
            this.rect[0] = Utils.lerp(this.rect[0], this.targetOffset[0], dt * this.scrollSpeed);
            this.rect[1] = Utils.lerp(this.rect[1], this.targetOffset[1], dt * this.scrollSpeed);
            ddraw.event();
        }
    }
}

export const surface = new Surface();