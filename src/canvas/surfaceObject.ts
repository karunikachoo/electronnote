import {Utils} from "../tools/utils";
import {SurfaceConst} from "./surfaceConst";
import {SurfaceObjectResizeHelper} from "./surfaceObjectResizeHelper";
import {KeyboardListener, stateMachine} from "../logic/stateMachine";
import {Cursor} from "./cursor";
import {surface} from "./surface";


export class SurfaceObject{
    static readonly OUTLINE_CL = '#256dff';
    static readonly OUTLINE_PADD = 10;

    id: string = `SurfaceObject.${Utils.genId(5)}`;
    parent: SurfaceObject;

    rect: number[] = [0, 0, 0, 0];  // x-offset, y-offset, width, height
    active: boolean = false;
    draggable: boolean = true;
    infinite: boolean = false;
    pannable: boolean = false;

    resizeable: boolean = true;
    isResizing: boolean = false;
    retainRatio: boolean = false;

    minWidth = 400;

    outlineColor:string = SurfaceObject.OUTLINE_CL;

    objects: SurfaceObject[] = [];
    activeObject: SurfaceObject = null;

    resizeHelper:SurfaceObjectResizeHelper = new SurfaceObjectResizeHelper(this);

    onChangeListener: (obj: SurfaceObject) => void;

    // TODO: Implement undo.

    constructor(parent?: SurfaceObject) {
        this.parent = parent;

        this._onContentChanged = this._onContentChanged.bind(this);
    }

    getTopParent(): SurfaceObject {
        if (this.parent == null)
            return this;
        else
            return this.parent.getTopParent();
    }

    getParent() {
        return this.parent ? this.parent : this;
    }

    addObject(obj: SurfaceObject) {
        obj.onChangeListener = this._onContentChanged;
        this.objects.push(obj);
    }

    removeObject(obj: SurfaceObject) {
        console.log(this.id, obj);
        const i = this.objects.indexOf(obj);
        if (i >= 0) {
            this.objects.splice(i, 1);
        }
    }

    _onContentChanged(obj: SurfaceObject) {
        return;
    }

    _deactivateChildren(exemptId?:string) {
        for (let i = 0; i < this.objects.length; i++) {
            const o = this.objects[i];
            if (o.id != exemptId) {
                o._deactivate();
                o._deactivateChildren(exemptId);
            }
        }
    }

    notifyActive(so: SurfaceObject) {
        this._deactivateChildren(so.id);
    }

    updateActive(so: SurfaceObject) {
        // console.log(this.id, so);
        this.activeObject = (so == this) ? null : so;
    }

    _activate() {
        if (!this.active) {
            this.active = true;
            this._deactivateChildren();
            this.getTopParent().notifyActive(this);
            this.getParent().updateActive(this);
        }
    }

    _deactivate() {
        if (this.active) {
            this.active = false;
        }
    }

    draw(ctx: CanvasRenderingContext2D,
         x: number, y: number, s: number, lim?:number) {

        // TODO: Move to webGL ?
        //  (not recommended, most of our shit is text anyway)

        this._drawActiveOutline(ctx, x, y, s);
        if (this.active && this.resizeable)
            this.resizeHelper.draw(ctx, x + this.rect[0], y + this.rect[1], s);

        const n = lim ? lim : this.objects.length;
        for(let i = 0; i < n; i++) {
            this.objects[i].draw(ctx, x + this.rect[0], y + this.rect[1], s);
        }

    }

    _drawActiveOutline(ctx: CanvasRenderingContext2D,
                       x: number, y: number, s: number) {
        if (this.active) {
            ctx.save()
            ctx.beginPath();
            ctx.strokeStyle = this.outlineColor;
            ctx.lineWidth = 2;
            ctx.rect(
                (x + this.rect[0]) * s - SurfaceObject.OUTLINE_PADD,
                (y + this.rect[1]) * s - SurfaceObject.OUTLINE_PADD,
                this.rect[2] * s + 2 * SurfaceObject.OUTLINE_PADD,
                this.rect[3] * s + 2 * SurfaceObject.OUTLINE_PADD,);
            ctx.stroke();
            ctx.restore();
        }
    }

    onChange() {
        if (this.onChangeListener) this.onChangeListener(this);
    }

    onClick(mId: number, ox:number, oy:number, s:number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];

        if (mId === 0) {
            const o = SurfaceObject._xyRectFilter(
                this.objects,
                ox + this.rect[0], oy + this.rect[1],
                x, y, s
            );
            if (o) {
                this._deactivate();
                // console.log(o.id, evt.pageX, evt.pageY, '|', x, y, '|', ox, oy);
                o.onClick(mId, ox + this.rect[0], oy + this.rect[1], s, evt);
            } else {
                this._activate();
            }
        }
    }

    onDoubleClick(mId: number, ox:number, oy:number, s:number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        if (mId === 0) {
            const o = SurfaceObject._xyRectFilter(
                this.objects,
                ox + this.rect[0], oy + this.rect[1],
                x, y, s
            );
            if (o) {
                // console.log(o.id, evt.pageX, evt.pageY, '|', x, y, '|', ox, oy);
                o.onDoubleClick(mId, ox + this.rect[0], oy + this.rect[1], s, evt);
            }
        }
    }

    onMouseUp(mId: number, ox: number, oy: number, s: number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        let o;

        if (this.activeObject == undefined) {
            o = SurfaceObject._xyRectFilter(
                this.objects,
                ox + this.rect[0], oy + this.rect[1],
                x, y, s
            );
        } else {
            o = this.activeObject;
        }
        if (!this.active && o) {
            o.onMouseUp(mId, ox, oy, s, evt);
        } else {
            if (this.isResizing) this.isResizing = false;
        }
    }

    onMouseMove(dxy: number[], ox: number, oy:number, s:number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        const o = SurfaceObject._xyRectFilter(
            this.objects,
            ox + this.rect[0], oy + this.rect[1],
            x, y, s
        );
        if (o) {
            Cursor.reset()
            o.onMouseMove(dxy,ox + this.rect[0], oy + this.rect[1], s, evt);
        } else {
            surface.updateHover(this);
            if (this.active && this.resizeable &&
                (this.resizeHelper.mouseCheck(
                    ox + this.rect[0], oy + this.rect[1],
                    x, y, s
                    ) ||
                    this.isResizing)) {
                Cursor.set(Cursor.SE_RESIZE);
            }
        }
    }

    onDrag(dxy: number[], ox: number, oy:number, s:number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        let o;

        if (this.activeObject == undefined) {
            o = SurfaceObject._xyRectFilter(
                this.objects,
                ox + this.rect[0], oy + this.rect[1],
                x, y, s
            );
        } else {
            o = this.activeObject;
        }

        if (!this.active && o) {
            Cursor.reset()
            o.onDrag(dxy, ox + this.rect[0], oy + this.rect[1], s, evt);
        } else {
            if (this.active) {
                if (this.resizeable && (this.resizeHelper.mouseCheck(ox + this.rect[0], oy + this.rect[1], x, y, s) || this.isResizing)) {
                    if (evt.shiftKey || this.retainRatio) {
                        if (Math.abs(dxy[0]) > Math.abs(dxy[1])) { // match x
                            const ratio = this.rect[3] / this.rect[2];
                            this.rect[2] += dxy[0] * SurfaceConst.FIDEL_MULT / s;
                            this.rect[3] = ratio * this.rect[2];
                        } else { // match y
                            const ratio = this.rect[2] / this.rect[3];
                            this.rect[3] += dxy[1] * SurfaceConst.FIDEL_MULT / s;
                            this.rect[2] = ratio * this.rect[3];
                        }
                    } else {
                        this.rect[2] += dxy[0] * SurfaceConst.FIDEL_MULT / s;
                        this.rect[3] += dxy[1] * SurfaceConst.FIDEL_MULT / s;
                        if (this.rect[2] < this.minWidth) this.rect[2] = this.minWidth;
                    }
                    this.isResizing = true;
                    Cursor.set(Cursor.SE_RESIZE);
                    this.onObjectResize(this.rect);
                } else if (this.draggable) {
                    this.rect[0] += dxy[0] * SurfaceConst.FIDEL_MULT / s;
                    this.rect[1] += dxy[1] * SurfaceConst.FIDEL_MULT / s;

                    if (!this.getParent().infinite) {
                        if (this.rect[0] + this.rect[2] > this.getParent().rect[2]) this.rect[0] = this.getParent().rect[2] - this.rect[2];
                        if (this.rect[0] < 0) this.rect[0] = 0;
                        if (this.rect[1] < 0) this.rect[1] = 0;
                    }

                    this.onObjectMove(this.rect);
                    Cursor.set(Cursor.GRABBING);
                }
                if (this.onChangeListener) this.onChangeListener(this);
            }

        }
    }

    onScroll(dxy: number[], ox: number, oy:number, s:number, evt: MouseEvent): boolean {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        let o = SurfaceObject._xyRectFilter(
                this.objects,
                ox + this.rect[0], oy + this.rect[1],
                x, y, s
            );

        if (o) {
            return o.onScroll(dxy, ox + this.rect[0], oy + this.rect[1], s, evt);
        } else {
            return this._pannableCheck(dxy, x, y, ox + this.rect[0], oy + this.rect[1], s);
        }
    }

    _pannableCheck(dxy:number[], x:number, y:number, ox:number, oy:number, s:number): boolean {
        return false;
    }

    onObjectMove(rect: number[]) {
        return;
    }

    onObjectResize(rect: number[]) {
        return;
    }

    static _xyRectFilter(objs: SurfaceObject[],
                         ox:number, oy:number,
                         mx:number, my:number, s:number) {
        for (let i = objs.length - 1; i >= 0; i--) {
            const o = objs[i];

            const [x0, x1] = [(ox + o.rect[0]) * s, (ox + o.rect[0] + o.rect[2]) * s];
            const [y0, y1] = [(oy + o.rect[1]) * s, (oy + o.rect[1] + o.rect[3]) * s];

            // console.log(`${o.id}: filter-x: ${x0} - ${x1} | filter-y: ${y0} - ${y1} | ${mx >= x0 && mx <= x1} : ${my >= y0 && my <= y1}`);

            if (mx >= x0 && mx <= x1 && my >= y0 && my <= y1) {
                return o;
            }
        }
        return null;
    }

    static visible(rect: number[], ox: number, oy: number,
                   ow:number, oh:number, s:number) {
        // TODO: add recursive visibility checks. ref _filterAndDrawObjects
        //  add permanent reference to surfaceObj top parent: Surface
    }

    toJSON(): Object {
        return {};
    }
}