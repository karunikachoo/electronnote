export class SurfaceConst {
    static readonly FIDEL_MULT = 2;
    static readonly ZOOM_MIN = 0.5;
    static readonly ZOOM_MAX = 5;

}