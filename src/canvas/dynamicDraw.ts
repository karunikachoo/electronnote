import {stateMachine} from "../logic/stateMachine";
import {db} from "../db/db";
import {animationHandler} from "./animationHandler";

class DynamicDraw {

    eventCounter: number = 1;
    lastDraw: number = 0;
    timeSum: number = 0;

    idleFPS = 5;
    fpsInterval = 1000 / this.idleFPS;

    start() {
        this._draw = this._draw.bind(this);
        this._draw();
    }

    event() {
        this.eventCounter++;
    }

    _draw() {
        const now = Date.now();
        const dt = now - this.lastDraw;
        if (this.eventCounter > 0) {
            this.eventCounter = 0;
            // this.eventCounter--;
            stateMachine.stateMachine();
        } else if (this.timeSum > this.fpsInterval){
            stateMachine.stateMachine();
            this.timeSum = 0;
        }
        animationHandler.animationNotify(dt);

        db.dbSaveCheck();

        this.timeSum += dt;
        this.lastDraw = now;
        requestAnimationFrame(this._draw)
    }
}

export const ddraw = new DynamicDraw();