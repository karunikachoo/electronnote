

export class Shapes {
    static roundedRect(
        ctx: CanvasRenderingContext2D,
        x: number, y: number,
        w: number, h: number,
        r: number) {

        if ( w <= 0 || h <= 0) {
            return;
        }

        ctx.beginPath();
        ctx.moveTo(x, y+r)
        ctx.arcTo(x, y, x+r, y, r);
        ctx.arcTo(x+w, y, x+w, y+r, r);
        ctx.arcTo(x+w, y+h, x+w-r, y+h, r);
        ctx.arcTo(x, y+h, x, y+h-r, r);
        ctx.lineTo(x, y+r);
    }

    static readonly RB_TOP = 0;
    static readonly RB_LEFT = 1;
    static readonly RB_RIGHT = 2;
    static readonly RB_BOTTOM = 3;

    static roundedBubble(ctx: CanvasRenderingContext2D,
                         x: number, y: number,
                         w: number, h: number,
                         r: number, d: number,
                         a: number, as:number) {
        if ( w <= 0 || h <= 0) {
            return;
        }

        ctx.beginPath();
        if (a == Shapes.RB_TOP) {
            ctx.moveTo(x, y+r);
            ctx.arcTo(x, y, x+r, y, r);

            // pointy
            ctx.lineTo(x + d - as/1.5, y);
            ctx.lineTo(x + d, y - as);
            ctx.lineTo(x + d + as/1.5, y);

            ctx.arcTo(x+w, y, x+w, y+r, r);
            ctx.arcTo(x+w, y+h, x+w-r, y+h, r);
            ctx.arcTo(x, y+h, x, y+h-r, r);
            ctx.lineTo(x, y+r);

        }

    }
}