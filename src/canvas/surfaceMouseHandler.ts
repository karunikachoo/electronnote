

export type dragHandler = (dxy: number[], evt: MouseEvent) => void;
export type clickHandler = (mouseID: number, evt: MouseEvent) => void;
export type zoomHandler = (dy: number, evt: MouseEvent) => void;


export class SurfaceMouseHandler {
    panHandler: dragHandler = null;
    dragHandler: dragHandler = null;
    clickHandler: clickHandler = null;
    doubleClickHandler: clickHandler = null;
    zoomHandler: zoomHandler = null;
    mouseUpHandler: clickHandler = null;
    mouseMoveHandler: dragHandler = null;

    e: HTMLCanvasElement;
    xy: number[] = [0, 0];
    down: boolean = false;
    moved: boolean = false;

    constructor(e: HTMLCanvasElement) {
        this.e = e;

        this.onDown = this.onDown.bind(this);
        this.onMove = this.onMove.bind(this);
        this.onUp = this.onUp.bind(this);
        this.onWheel = this.onWheel.bind(this);
        this.onDoubleClick = this.onDoubleClick.bind(this);

        this.e.onmousedown = this.onDown;
        this.e.onmousemove = this.onMove;
        this.e.onmouseup = this.onUp;
        this.e.onwheel = this.onWheel;
        this.e.ondblclick = this.onDoubleClick;
    }

    onWheel(evt: WheelEvent) {
        if (evt.ctrlKey) { // zoom
            if (this.zoomHandler) this.zoomHandler(-evt.deltaY * 0.025, evt);
        } else {
            if (this.panHandler)
                this.panHandler([-evt.deltaX * 0.5, -evt.deltaY * 0.5], evt);
        }
    }

    onDoubleClick(evt: MouseEvent) {
        this.doubleClickHandler(evt.button, evt);
    }

    onDown(evt: MouseEvent) {
        this.xy[0] = evt.pageX;
        this.xy[1] = evt.pageY;

        this.down = true;
        this.moved = false;
    }

    onMove(evt: MouseEvent) {
        const dxy = [evt.pageX - this.xy[0], evt.pageY - this.xy[1]];
        this.xy[0] = evt.pageX;
        this.xy[1] = evt.pageY;
        if (this.down) {
            // const dxy = [evt.pageX - this.xy[0], evt.pageY - this.xy[1]];
            // this.xy[0] = evt.pageX;
            // this.xy[1] = evt.pageY;
            if (!this.moved) this.moved = true;
            if (this.dragHandler) this.dragHandler(dxy, evt);
        } else {
            if (this.mouseMoveHandler) this.mouseMoveHandler(dxy, evt);
        }
    }

    onUp(evt: MouseEvent) {
        this.down = false;
        if (!this.moved) {
            if (this.clickHandler) this.clickHandler(evt.button, evt);
        } else {
            if (this.mouseUpHandler) this.mouseUpHandler(evt.button, evt);
        }
    }
}