import {fsPanel} from "../ui/fsPanel";


export class Tool {
    e: HTMLCanvasElement;
    active:boolean = false;
    oribg:string = "#22222288";

    constructor(id: string) {
        this.e = document.getElementById(id) as HTMLCanvasElement;
        this.oribg = this.e.style.background;

        this.onClick = this.onClick.bind(this);

        this.e.onclick = this.onClick;

        this.updateSize();
    }

    activate() {
        this.active = true;
        this.e.style.backgroundColor = "#35445e"
    }

    deactivate() {
        this.active = false;
        this.e.style.backgroundColor = this.oribg;
    }

    onClick(evt: MouseEvent) {
        fsPanel.hide();
        this.activate();
        return;
    }

    draw() {
        const ctx = this.e.getContext('2d');
        const rect = this.e.getBoundingClientRect();
        const w = rect.width;
        const h = rect.height;

        this._drawOverlay(ctx, w, h);
    }

    _drawOverlay(ctx: CanvasRenderingContext2D, w: number, h: number) {
        return;
    }

    updateSize() {
        const rect = this.e.getBoundingClientRect();
        this.e.width = rect.width;
        this.e.height = rect.height;

        this.draw();
    }
}