import {Tool} from "./tool";
import {db} from "../db/db";


export class SaveNote extends Tool{

    onClick(evt: MouseEvent) {
        db.saveNotes();
    }

    _drawOverlay(ctx: CanvasRenderingContext2D, w: number, h: number) {
        ctx.strokeStyle = '#fff';
        ctx.lineCap = 'round';

        const s = w/8;

        ctx.beginPath();
        ctx.moveTo(w/2 - 2 * s, h/2 + 2 * s);
        ctx.lineTo(w/2 + 1.25 * s, h/2 + 2 * s);
        ctx.lineTo(w/2 + 2 * s, h/2 + 1.25 * s);
        ctx.lineTo(w/2 + 2 * s, h/2 - 2 * s);
        ctx.lineTo(w/2 - 2 * s, h/2 - 2 * s);
        ctx.closePath();
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(w/2 - s, h/2 + 2 * s);
        ctx.lineTo(w/2 - s, h/2 + 1.125 * s);
        ctx.lineTo(w/2 + 0.5 * s, h/2 + 1.125 * s);
        ctx.lineTo(w/2 + 0.5 * s, h/2 + 2 * s);
        ctx.moveTo(w/2 - 0.5 * s, h/2 + 2 * s);
        ctx.lineTo(w/2 - 0.5 * s, h/2 + 1.125 * s);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(w/2 - 1.25 * s, h/2 - 2 * s);
        ctx.lineTo(w/2 - 1.25 * s, h/2 );
        ctx.lineTo(w/2 + 1.25 * s, h/2 );
        ctx.lineTo(w/2 + 1.25 * s, h/2 - 2 * s);
        ctx.stroke();
    }
}