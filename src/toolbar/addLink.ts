import {Tool} from "./tool";
import {StateMachine, stateMachine} from "../logic/stateMachine";
import {FItem, fsPanel} from "../ui/fsPanel";
import {Cursor} from "../canvas/cursor";
import {Text} from "../objects/text/text";
import {MdText} from "../objects/text/mdText";


export class AddLink extends Tool{

    onClick(evt: MouseEvent) {
        let o = stateMachine.getLatestKeyboardListener();

        if (o instanceof MdText) {
            let linkText = ""
            if (o.caret.hasSelection()) {
                linkText = o.caret.getSelection(o.editText);
            }

            // const carets = o.caret.get();
            stateMachine.removeKeyboardLister(o);
            fsPanel.onClickCallback = (id) => {
                if (o instanceof MdText) {
                    stateMachine.addKeyboardListener(o);
                    o.editing = true;
                    const linkMd = id ? `[${linkText}](\$${id})` : `[${linkText}]()`;
                    o.insert(linkMd);
                    o.caret.prev(o.editText, id ? (linkMd.length-1) : 1);
                }
            }
            fsPanel.show();
        }


    }

    _drawOverlay(ctx: CanvasRenderingContext2D, w: number, h: number) {
        ctx.strokeStyle = '#fff';
        ctx.lineCap = 'round';

        const s = w/8;

        ctx.beginPath();
        ctx.arc(w/2, h/2, s/1.5, -Math.PI/6, -Math.PI * 3/4, true);
        ctx.arc(w/2 - s, h/2 + s, s/1.5, -0.75 * Math.PI, 0.25 * Math.PI, true);
        ctx.lineTo(w/2 - 0.1 * s, h/2 + 1.1 * s);
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(w/2, h/2, s/1.5, 0.75 * Math.PI, 0.25 * Math.PI, true);
        ctx.arc(w/2 + s, h/2 - s, s/1.5, 0.25 * Math.PI, -0.75 * Math.PI, true);
        ctx.lineTo(w/2 + 0.1 * s, h/2 - 1.1 * s);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(w/3, h/3-s);
        ctx.lineTo(w/3, h/3+s);
        ctx.moveTo(w/3-s, h/3);
        ctx.lineTo(w/3+s, h/3);
        ctx.stroke();
    }
}