import {Tool} from "./tool";
import {StateMachine, stateMachine} from "../logic/stateMachine";
import {fsPanel} from "../ui/fsPanel";
import {Cursor} from "../canvas/cursor";


export class AddNote extends Tool{

    onClick(evt: MouseEvent) {
        super.onClick(evt);
        Cursor.set(Cursor.CROSSHAIR);
        stateMachine.stateId = StateMachine.ST_ADD_NOTE
    }

    _drawOverlay(ctx: CanvasRenderingContext2D, w: number, h: number) {
        ctx.strokeStyle = '#fff';
        ctx.lineCap = 'round';

        const s = w/8;

        ctx.beginPath();
        ctx.moveTo(w/3, h/3-s);
        ctx.lineTo(w/3, h/3+s);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(w/3-s, h/3);
        ctx.lineTo(w/3+s, h/3);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(w/2 - s/2, h/4);
        ctx.lineTo(3/4 * w, h/4);
        ctx.lineTo(3/4 * w, 2/4 * h + s/2);
        ctx.lineTo(w * 2/4 + s/2, h * 3/4);
        ctx.lineTo(1/4 * w, 3/4 * h);
        ctx.lineTo(1/4 * w,  h/2 - s/2);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(w * 2/4 + s/2, h * 3/4);
        ctx.lineTo(w * 2/4 + s/2, h * 2/4 + s/2);
        ctx.lineTo(w * 3/4, h * 2/4 + s/2);
        ctx.stroke();
    }
}