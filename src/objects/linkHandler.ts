import {SurfaceConst} from "../canvas/surfaceConst";
import {Cursor} from "../canvas/cursor";
import {surface} from "../canvas/surface";
import {db} from "../db/db";
import {Note} from "./note";
import {SurfaceObject} from "../canvas/surfaceObject";
import {shell} from 'electron';

export class Link {
    rect: number[] = [0, 0, 0, 0];
    url: string;
}

/**
 * Global Handling
 * Tracking by MdText.id
 * each draw call from MdText is bundled together and sent to
 * the linkHandler. Link handler will replace it with new links
 *
 */


export class LinkHandler {
    links: Map<string, Link[]> = new Map();
    updatedIds: string[] = [];

    removeFrom(parentId: string) {
        if (this.links.has(parentId)) {
            this.links.delete(parentId);
        }
    }

    updateLinks(parentID: string, links: Link[]) {
        this.links.set(parentID, links);
    }

    drawLinks(ctx: CanvasRenderingContext2D,
              ox: number, oy: number, s: number) {

        this.links.forEach(links => {
            links.forEach(link => {
                if (link.url.includes('$')) {        // internal URL
                    const id = link.url.replace("$", "");
                    if (surface.has(id)) {  // Note is open
                        this._drawLink(ctx, ox, oy, s, link,
                            surface.getObject(id));
                    }
                }
            })
        });
    }

    _drawLink(ctx: CanvasRenderingContext2D,
              ox: number, oy: number, s: number,
              link:Link, note: SurfaceObject) {
        const [lx, ly, lw, lh] = link.rect;
        const [nx, ny, nw, nh] = note.rect;

        // Default right mid to left mid
        const [x0, y0] = [ox + lx, oy + ly];
        const [x1, y1] = [x0 + lw, y0 + lh];

        const [x2, y2] = [ox + nx, oy + ny];
        const [x3, y3] = [x2 + nw, y2 + nh];

        /**
         *  IN x you have 3 positions
         *   |              |
         *   V      v      v
         *  left   mid   right
         */

        let [cx0, cx1, cx2, cx3] = [-0, -0, -0, -0];
        let [cy0, cy1, cy2, cy3] = [-0, -0, -0, -0];

        if ((x2 < (x1/2 + x0/2) && (x1/2 + x0/2) < x3)) {  // switch to mid
            // console.log("mid-mid");
            cx0 = (x1/2 + x0/2);
            cx1 = cx0;
            cx3 = (x2 + x3) / 2;
            cx2 = cx3

            if (y3 < y0) {  // above
                cy0 = y0;
                cy3 = y3
                cy1 = (cy3 + cy0) / 2;
                cy2 = cy1;
            } else if (y2 > y1) {   // below
                cy0 = y1;
                cy3 = y2;
                cy1 = (cy3 + cy0) / 2;
                cy2 = cy1;
            } else {
                return;
            }
        } else if (x2 < x1 && x1 < x3) {
            // console.log("mid-left")
            cx0 = (x1/2 + x0/2);
            cx1 = cx0;
            cx3 = x2;
            cx2 = (cx3 + cx0) / 2;

            if (y3 < y0) {
                cy0 = y0;
                cy3 = (y2 + y3) / 2;

                cy1 = (cy3 + cy0) / 2;
                cy2 = cy3;
            } else if (y2 > y1) {
                cy0 = y1;
                cy3 = (y2 + y3) / 2;

                cy1 = (cy3 + cy0) / 2;
                cy2 = cy3;
            } else {
                return;
            }
        } else if (x2 < x0 && x0 < x3) {
            // console.log("mid-right");
            cx0 = (x1/2 + x0/2);
            cx1 = cx0;
            cx3 = x3;
            cx2 = (cx3 + cx0) / 2;

            if (y3 < y0) {
                cy0 = y0;
                cy3 = (y2 + y3) / 2;

                cy1 = (cy3 + cy0) / 2;
                cy2 = cy3;
            } else if (y2 > y1) {
                cy0 = y1;
                cy3 = (y2 + y3) / 2;

                cy1 = (cy3 + cy0) / 2;
                cy2 = cy3;
            } else {
                return;
            }

        } else if (x3 < x0) {
            // console.log("left-right");
            cx0 = x0;
            cx3 = x3
            cx1 = (cx0 + cx3) / 2;
            cx2 = (cx0 + cx3) / 2;

            cy0 = (y0 + y1) / 2;
            cy3 = (y2 + y3) / 2;

            cy1 = cy0;
            cy2 = cy3;
        } else if (x2 > x1) {
            // console.log("right-left");
            cx0 = x1;
            cx3 = x2;
            cx1 = (cx0 + cx3) / 2;
            cx2 = (cx0 + cx3) / 2;

            cy0 = (y0 + y1) / 2;
            cy3 = (y2 + y3) / 2;

            cy1 = cy0;
            cy2 = cy3;
        } else {
            return;
        }

        ctx.beginPath();
        // ctx.strokeStyle = "#e7700888";
        // ctx.fillStyle = "#e7700888";
        ctx.strokeStyle = (note as Note).accent;
        ctx.fillStyle = ctx.strokeStyle;
        ctx.lineWidth = 3 * s;
        ctx.moveTo(cx0 * s, cy0 * s);
        ctx.bezierCurveTo(
            cx1 * s, cy1 * s,
            cx2 * s, cy2 *s,
            cx3 * s, cy3 * s );
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(cx3 * s, cy3 * s, 10 * s, 0, 2 * Math.PI);
        ctx.fill();
    }

    _checkLinks(x:number, y:number): Link {
        let ret = null;

        const [ofx, ofy] = surface.getOffset();

        this.links.forEach(links => {
            links.forEach(link => {
                const [x0, y0] = [ofx + link.rect[0], ofy + link.rect[1]];
                const [x1, y1] = [x0 + link.rect[2], y0 + link.rect[3]];

                if (x >= x0 && x <= x1 && y >= y0 && y <= y1) {
                    ret = link;
                }
            })
        })
        return ret;
    }

    onHover(ox:number, oy: number, s:number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT / s, evt.pageY * SurfaceConst.FIDEL_MULT / s];
        const link = this._checkLinks(x, y);
        if (link) {
            Cursor.set(Cursor.POINTER);
            return true;
        } else {
            return false;
        }
    }

    onClick(mId: number, ox:number, oy: number, s:number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT / s, evt.pageY * SurfaceConst.FIDEL_MULT / s];
        const link = this._checkLinks(x, y);
        if (link) {
            this.onLinkClick(link);
            return true;
        } else {
            return false;
        }
    }

    onLinkClick(link: Link) {
        if (link.url.includes('$')) {        // internal URL
            const id = link.url.replace("$", "");
            const check = surface.has(id);
            const note = db.openNote(id)
            // if (!check && note) note.compress();
        } else {                        // external URL
            // open in browser
            shell.openExternal(link.url);
        }
    }
}

export const linkHandler = new LinkHandler();