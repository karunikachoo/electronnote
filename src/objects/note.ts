import {SurfaceObject} from "../canvas/surfaceObject";
import {Shapes} from "../canvas/shapes";
import {Text} from "./text/text";
import {Utils} from "../tools/utils";
import {MdText} from "./text/mdText";
import {SurfaceObjectKB} from "../canvas/surfaceObjectKB";
import {dialogBox} from "../ui/dialogBox";
import {SurfaceConst} from "../canvas/surfaceConst";
import {db, DBNote} from "../db/db";
import {Cursor} from "../canvas/cursor";
import {clipboard} from "electron";
import {TokenComponents} from "./text/tokenComponents";
import {ddraw} from "../canvas/dynamicDraw";
import {Link, linkHandler} from "./linkHandler";
import {Colors} from "../tools/colors";
import {SurfaceImage} from "./surfaceImage";


export class Note extends SurfaceObjectKB {

    static readonly RADIUS = 15;
    static readonly PADD = [15, 15];

    static readonly FS_NORMAL = 24;
    static readonly FS_TITLE = 36;

    id: string = `${Utils.genId(8)}`;

    bg: string = "#e5e5e5";
    br: string = "#888888";
    tc: string = "#333333";
    // accent: string = "#ff8e15";
    accent: string = Colors.random();

    title: Text = new Text(
        this,
        2 * Note.PADD[0], 0,
        this.rect[0] + this.rect[2] - Note.PADD[0]);

    sections: MdText[] = [];

    closeBtnHover: boolean = false;
    addSectionHover: boolean = false;
    expandHover = false;

    compressed = false;

    links: Map<string, Link> = new Map<string, Link>();

    constructor(parent:SurfaceObject, x:number, y:number, w:number, h:number, sections?:string[], id?:string) {
        super(parent);

        if (id) this.id = id;

        this.rect = [x, y, w, h];

        this._onContentChanged = this._onContentChanged.bind(this);
        this.removeObject = this.removeObject.bind(this);
        this._nextSection = this._nextSection.bind(this);
        this._newSection = this._newSection.bind(this);
        this._prevSection = this._prevSection.bind(this);
        this._onEmptyDelete = this._onEmptyDelete.bind(this);

        this.title.fontSize = Note.FS_TITLE;
        this.title.fontWeight = 600;
        this.title.lineHeight = 1.25;
        this.title.hint = 'Title';
        this.title.onChangeListener = this._onContentChanged;
        this.objects.push(this.title);

        this._saveNote = this._saveNote.bind(this);

        this.initSections(sections);
    }

    compress() {
        if (this.sections.length > 1) {
            this.compressed = true;
            this.expandHover = false;
            this.sections.slice(1).forEach(s => {
                linkHandler.removeFrom(s.id)
            })
        }
    }

    expand() {
        this.compressed = false;
        this.expandHover = false;
    }

    /** Handle Sections */
    initSections(texts?: string[]) {
        this.sections.forEach(o => {
            this.removeObject(o);
        })
        this.sections = [];
        const w = this.rect[0] + this.rect[2] - Note.PADD[0];
        const x = 2 * Note.PADD[0];
        let y = 0.4 * Note.PADD[1] + this.title.rect[1] + this.title.rect[3] + 1.5 * Note.PADD[1];
        if (texts) {
            for (let i = 0; i < texts.length; i++) {
                const text = texts[i];
                this._addSection(x, y, w, text);
            }
        } else {
            this._addSection(x, y, w);
        }
    }

    _addSection(x:number, y:number, w:number, text:string="", index:number=-1) {
        const n = new MdText(this, x, y, w);
        n.fontSize = Note.FS_NORMAL;
        n.fontWeight = 200;
        n.text = text;
        n.hint = "Section content";
        n.deletable = this.sections.length > 0;
        n.onChangeListener = this._onContentChanged;

        n.onSaveRequest = this._saveNote;
        n.onObjectRemoveRequest = this.removeObject;
        n.onNextSibling = this._newSection;
        // n.onNextSibling = this._nextSection;
        n.onPrevSibling = this._prevSection;
        n.onEmptyDelete = this._onEmptyDelete;

        if (index >= 0) {
            const bfore = this.sections.slice(0, index);
            const after = this.sections.slice(index);
            this.sections = [...bfore, n, ...after];
        } else {
            this.sections.push(n);
        }

        this.objects.push(n);
        return n;
    }

    addSectionAndSelect(obj?: SurfaceObject) {
        let sect;

        if (obj) {
            console.log(obj.id);


            let n = 0;
            for (let i = 0; i < this.sections.length; i++) {
                if (this.sections[i].id == obj.id) {
                    n = i;
                    break;
                }
            }

            if (n >= 0) {
                sect = this._addSection(0, 0, 0, "", n + 1);
            } else {
                sect = this._addSection(0, 0, 0);
            }
        } else {
            sect = this._addSection(0, 0, 0);
        }
        sect.editing = true;
        sect._activate();
    }

    _newSection(obj?: Text) {
        let sect;
        let afterText = "";
        let beforeText = "";

        if (obj) {
            let n = 0;
            for (let i = 0; i < this.sections.length; i++) {
                if (this.sections[i].id == obj.id) {
                    n = i;
                    break;
                }
            }

            const split =  obj.caret.getSplit(obj.editText);
            afterText = split.after;
            beforeText = split.before;

            sect = this._addSection(0, 0, 0, afterText, n+1);

        }
        sect.editing = true;
        // sect.text = afterText + sect.text;
        sect._activate();

        if (obj) obj.text = beforeText;
    }

    _nextSection(obj?: Text) {
        let sect;
        let afterText = "";
        let beforeText = "";

        if (obj) {
            let n = 0;
            for (let i = 0; i < this.sections.length; i++) {
                if (this.sections[i].id == obj.id) {
                    n = i;
                    break;
                }
            }

            const split =  obj.caret.getSplit(obj.editText);
            afterText = split.after;
            beforeText = split.before;

            if (n >= 0 && n + 1 < this.sections.length) {
                sect = this.sections[n+1];
            } else {
                sect = this._addSection(0, 0, 0);
            }
        }
        sect.editing = true;
        sect.text = afterText + sect.text;
        sect._activate();

        if (obj) obj.text = beforeText;
    }

    _prevSection(obj?: SurfaceObject) {
        let sect;
        if (obj) {
            let n = 0;
            for (let i = 0; i < this.sections.length; i++) {
                if (this.sections[i].id == obj.id) {
                    n = i;
                    break;
                }
            }

            if (n >= 0 && n - 1 >= 0) {
                sect = this.sections[n-1];
                sect.editing = true;
                sect._activate();
            }
        }
    }

    _onEmptyDelete(obj: Text) {
        const txt = obj.text;
        let n = 0;

        for (let i = 0; i < this.sections.length; i++) {
            if (this.sections[i].id == obj.id) {
                n = i;
                break;
            }
        }

        if (n > 0) {
            const sect = this.sections[n-1];
            sect.text += txt;
            sect.editing = true;
            sect.caret.toEnd(sect.editText);
            sect._activate();
        }

        this.removeObject(obj);

    }

    getSections() {
        return this.sections.map(o => {
            return o.text;
        })
    }

    /** Objects */
    // TODO: HANDLE OBJECT PARSING
    parseObjects(objs: Object[]) {
        objs.forEach(o => {
            // console.log(o);
            if (o.hasOwnProperty('img')) {
                const im = new SurfaceImage(this, 0, 0);
                im.fromObj(o);
                this.addObject(im);
            }
        })
        return;
    }

    getJSONObjects(): Object[] {
        let ret: Object[] = [];
        this.objects.forEach(o => {
            if (!(o instanceof MdText && this.sections.includes(o))) {
                ret.push(o.toJSON());
            }
        })
        return ret;
    }

    onObjectResize(rect: number[]) {
        this._onContentChanged(this);
    }

    removeObject(obj: SurfaceObject) {
        super.removeObject(obj);
        const i =  this.sections.indexOf(obj as MdText);
        if (i >= 0) {
            this.sections.splice(i, 1);
        }
    }

    /** Note */
    _onContentChanged(obj: SurfaceObject) {
        db.updateNote(this);
    }

    _saveNote() {
        db.saveNote(this.id);
    }

    /** Drawing */
    _updateTextDimensions() {
        const w = this.rect[2] - 4.9 * Note.PADD[0];
        let off = 1.75 * Note.PADD[1];

        const x = 2.5 * Note.PADD[0];
        this.title.updateWidth(w - 3 * Note.PADD[0]);
        this.title.rect[0] = x
        this.title.rect[1] = off;

        off += this.title.rect[3] + 1.5 * Note.PADD[1];

        const n = this.compressed ? 1 : this.sections.length;
        for (let i = 0; i < n; i++) {
            this.sections[i].updateWidth(w);
            this.sections[i].rect[0] = x;
            this.sections[i].rect[1] = off;
            off += this.sections[i].rect[3] + 3 * Note.PADD[1];
        }


        // if (off > this.rect[3])

        for (let i = 0; !this.compressed && i < this.objects.length; i++) {
            const o = this.objects[i];
            const oh = o.rect[1] + o.rect[3];
            const ow = o.rect[0] + o.rect[2];
            if (oh > off) off = oh;
        }

        off += 1.5 * Note.PADD[1];
        this.rect[3] = off;
    }

    draw(ctx: CanvasRenderingContext2D,
         x: number, y: number, s: number) {

        this._updateTextDimensions();
        // Show that it is unsaved
        this.title.setSuffix(db.isNoteSaved(this.id) ? "" : " *");

        this._drawFullBg(ctx, x, y, s);
        this._drawDividingLines(ctx, x, y, s);
        this._drawCloseButton(ctx, x, y, s);
        this._drawAddSectionButton(ctx, x, y, s);
        this._drawExpandCompress(ctx, x, y, s);

        const n = this.objects.indexOf(this.sections[0]);
        super.draw(ctx, x, y, s, this.compressed ? n+1 : null);
    }

    _drawExpandCompress(ctx: CanvasRenderingContext2D,
                        ox: number, oy: number, s: number) {
        const x0 = ox + this.rect[0] + 1.5 * Note.PADD[0];
        const y1 = oy + this.rect[1] + this.rect[3] - 1.5 * Note.PADD[1];

        ctx.save();

        if (this.expandHover) {
            ctx.fillStyle = "#33333322";
            TokenComponents._pathRoundRect(ctx, x0 - 10, y1 - 30, 40, 40, 5, s);
            ctx.fill();
        }

        ctx.beginPath();
        const v = 20;
        ctx.lineWidth = 3 * s;
        if (this.compressed) {
            ctx.moveTo(x0 * s, (y1 - v/2) * s);
            ctx.lineTo(x0 * s, y1 * s);
            ctx.lineTo((x0 + v/2) * s, y1 * s);
            ctx.moveTo(x0 * s, y1 * s);
            ctx.lineTo((x0 + v) * s, (y1 - v) * s);
            ctx.moveTo((x0 + v/2) * s, (y1 - v) * s);
            ctx.lineTo((x0 + v) * s, (y1 - v) * s);
            ctx.lineTo((x0 + v) * s, (y1 - v/2) * s);

            ctx.stroke();
        } else {
            ctx.moveTo(x0 * s, y1 * s);
            ctx.lineTo((x0 + v) * s, (y1 - v) * s);

            ctx.moveTo((x0 + v/2.5) * s, y1 * s);
            ctx.lineTo((x0 + v/2.5) * s, (y1 - v/2.5) * s);
            ctx.lineTo((x0) * s, (y1 - v/2.5) * s);

            ctx.moveTo((x0 + v * 0.6) * s, (y1 - v) * s);
            ctx.lineTo((x0 + v * 0.6) * s, (y1 - v * 0.6) * s);
            ctx.lineTo((x0 + v) * s, (y1 - v * 0.6) * s);
            ctx.stroke();
        }
        ctx.restore();
    }

    _drawAddSectionButton(ctx: CanvasRenderingContext2D,
    ox: number, oy: number, s: number) {
        const x0 = ox + this.rect[0] + this.rect[2]/2 - 10;
        const y0 = oy + this.rect[1] + this.rect[3] - 1.5 * Note.PADD[1] - 20;

        if (this.compressed) return;

        ctx.save();
        if (this.addSectionHover) {
            ctx.fillStyle = "#33333322";
            TokenComponents._pathRoundRect(ctx, x0 - 10, y0 - 10, 40, 40, 5, s);
            ctx.fill();
        }

        ctx.beginPath();
        ctx.fillStyle = "#333333";
        ctx.lineWidth = 3 * s;
        ctx.moveTo((x0 + 10) * s, y0 * s);
        ctx.lineTo((x0 + 10)  * s, (y0 + 20) * s);
        ctx.moveTo(x0 * s, (y0 + 10) * s);
        ctx.lineTo((x0 + 20) * s, (y0 + 10) * s);
        ctx.stroke();
        ctx.restore();
    }

    _drawCloseButton(ctx: CanvasRenderingContext2D,
                     ox: number, oy: number, s: number) {
        const x1 = ox + this.rect[0] + this.rect[2] - 1.5 * Note.PADD[0];
        const y1 = oy + this.rect[1] + 1.5 * Note.PADD[1];

        ctx.save();
        if (this.closeBtnHover) {
            ctx.fillStyle = "#33333322";
            TokenComponents._pathRoundRect(ctx, x1 - 30, y1 - 10, 40, 40, 5, s);
            ctx.fill();
        }

        ctx.beginPath();
        ctx.fillStyle = "#333333";
        ctx.lineWidth = 3 * s;
        ctx.moveTo((x1) * s, (y1) * s );
        ctx.lineTo((x1 - 20) * s, (y1 + 20) * s);
        ctx.moveTo((x1) * s, (y1 + 20) * s );
        ctx.lineTo((x1 - 20) * s, (y1) * s);
        ctx.stroke();
        ctx.restore();
    }

    _drawDividingLines(ctx: CanvasRenderingContext2D,
                       ox: number, oy: number, s:number) {
        const x0 = ox + this.rect[0];
        const x1 = x0 + this.rect[2];

        if (this.sections.length > 1 && !this.compressed) {
            for (let i = 1; i < this.sections.length; i++) {
                const y = oy + this.rect[1] + this.sections[i].rect[1] - 1.5 * Note.PADD[1];
                ctx.beginPath();
                ctx.strokeStyle = "#33333388";
                ctx.lineWidth = s;
                ctx.moveTo(x0 * s, y * s);
                ctx.lineTo(x1 * s, y * s);
                ctx.stroke();
            }
        }
    }

    _drawFullBg(ctx: CanvasRenderingContext2D,
                x: number, y: number, s: number) {
        ctx.fillStyle = this.bg;
        ctx.strokeStyle = this.br;
        ctx.lineWidth = s;
        Shapes.roundedRect(ctx,
            (x + this.rect[0]) * s, (y + this.rect[1]) * s,
            this.rect[2] * s, this.rect[3] * s,Note.RADIUS * s);
        ctx.fill();
        ctx.stroke();
    }

    /** Input Handling */
    onClick(mId: number, ox: number, oy: number, s: number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT / s, evt.pageY * SurfaceConst.FIDEL_MULT / s];

        if (this._closeButtonCheck(ox, oy, x, y)) {
            this.sections.forEach(s => {
                linkHandler.removeFrom(s.id)
            })
            this.parent.removeObject(this);
            db.reloadDirectory(true);
        } else if (this._addSectionButtonCheck(ox, oy, x, y)) {
            this.addSectionAndSelect();
        } else if (this._expandCompressCheck(ox, oy, x, y)) {
            if (this.compressed) this.expand();
            else this.compress();
        } else {
            super.onClick(mId, ox, oy, s, evt);
        }
    }

    onMouseMove(dxy: number[], ox: number, oy: number, s: number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT / s, evt.pageY * SurfaceConst.FIDEL_MULT / s];

        if (this._closeButtonCheck(ox, oy, x, y)) {
            Cursor.set(Cursor.POINTER);
            this.closeBtnHover = true;
            ddraw.event();

        } else if (this._addSectionButtonCheck(ox, oy, x, y)) {
            Cursor.set(Cursor.POINTER);
            this.addSectionHover = true;
            ddraw.event();
        } else if (this._expandCompressCheck(ox, oy, x, y)) {
            Cursor.set(Cursor.POINTER);
            this.expandHover = true;
            ddraw.event();
        } else {
            this.expandHover = false;
            this.addSectionHover = false;
            this.closeBtnHover = false;
            Cursor.reset();
            super.onMouseMove(dxy, ox, oy, s, evt);
        }
    }

    _expandCompressCheck(ox: number, oy: number, x: number, y: number) {
        const x0 = ox + this.rect[0] + 1.5 * Note.PADD[0] - 10;
        const y1 = oy + this.rect[1] + this.rect[3] - 1.5 * Note.PADD[1] + 10;
        const y0 = y1 - 40;
        const x1 = x0 + 40;

        return x >= x0 && x <= x1 && y >= y0 && y <= y1;
    }

    _addSectionButtonCheck(ox: number, oy:number, x:number, y:number) {
        const x0 = ox + this.rect[0] + this.rect[2]/2 - 20;
        const y0 = oy + this.rect[1] + this.rect[3] - 1.5 * Note.PADD[1] - 30;
        const x1 = x0 + 40;
        const y1 = y0 + 40;

        return x >= x0 && x <= x1 && y >= y0 && y <= y1;
    }

    _closeButtonCheck(ox: number, oy:number, x:number, y:number) {
        const x1 = ox + this.rect[0] + this.rect[2] - 1.5 * Note.PADD[0];
        const x0 = x1 - 20;
        const y0 = oy + this.rect[1] + 1.5 * Note.PADD[1];
        const y1 = y0 + 20;

        return x >= x0 && x <= x1 && y >= y0 && y <= y1;
    }

    onKeyDown(e: KeyboardEvent) {
        if (e.key == 'Backspace' && e.metaKey || e.key == 'Delete') {
            dialogBox.setText('Delete Note?', 'References to this note may be lost. You will not be able to recover this note.');
            dialogBox.show(res => {
                if (res) {
                    this.sections.forEach(s => {
                        linkHandler.removeFrom(s.id)
                    })

                    this.parent.removeObject(this);
                    db.deleteNote(this.id);
                }
            })
        } else if (e.metaKey || e.ctrlKey) {
            if(e.key == 's') {
                this._saveNote();
            }
            if (e.key == 'v') {
                const img = clipboard.readImage();
                console.log(img, img.isEmpty(), img.getSize());
                if (!img.isEmpty()) {
                    console.log(img.toJPEG(100).toJSON());
                    const si = new SurfaceImage(this, 10, 10);
                    si.fromNativeImage(img);
                    this.objects.push(si);
                }

            }
        }
    }
}