import {MdConst} from "./mdConst";

export class Token {
    static readonly BOLD = 0x1;                     // fw: 600
    static readonly ITALIC = 0x1 << 1;              // style: italic
    static readonly UNDERLINE = 0x1 << 2;           // additional draw
    static readonly STRIKETHROUGH = 0x1 << 3;       // additional draw
    static readonly HEADING = 0x1 << 4;             // fs: 24-48px
    static readonly LINK = 0x1 << 5;                // color: blue
    static readonly INLINE_CODE = 0x1 << 6;         // font: monospace + 2 spaces before and after
    static readonly CODE_BLOCK = 0x1 << 7;          // font draw offsets
    static readonly RETURN = 0x1 << 8;              // carriage return

    text: string;
    type: number = 0x0;
    data: string | number;
    // ctxfont: string;

    constructor(text: string = '') {
        this.text = text;
    }

    copy() {
        const t = new Token(this.text);
        t.type |= this.type;
        if (this.data) t.data = this.data;
        // if (this.ctxfont) t.ctxfont = this.ctxfont;

        return t;
    }
}


export class MdParser {
    static endIndex(text: string, c: string) {
        const i = text.indexOf(c);
        return i == -1 ? text.length : i;
    }

    static consume(text: string, ref: string[]): [string, string] {
        const end = Math.min(...ref.map((v) => {
            return MdParser.endIndex(text, v);
        }));

        const part = text.slice(0, end);
        text = text.slice(end);

        return [part, text];
    }

    static consumeTo(text: string, c: string, tknOff: number=1, eoff: number=0): [string, string] {
        const end = text.indexOf(c, 1);
        if (end == -1)
            throw new Error(`Missing closing ${c}`);
        const part = text.slice(tknOff, end + eoff);
        text = text.slice(end + c.length);
        return [part, text];
    }

    static _errorConsume(text: string, tokenLen: number) {
        // console.log('errorConsume');
        let token = text.slice(0, tokenLen);
        text = text.slice(tokenLen);
        const [part, txt] = MdParser.consume(text, MdConst.SPECIAL_CHARS);
        return [token + part, txt];
    }

    static parse(text: string) {
        let count = 0;
        let tokens: Token[] = [];

        while (text != '') {
            if (count > 2000){
                console.log('loop limit reached');
                break;
            }
            count++;

            const c = text.charAt(0);

            // TODO: add handling for horizontal section line ___ or ---?

            if (c === '\\') {
                const [rep, txt] = MdParser._escape(text);
                if (tokens.length > 0) tokens[tokens.length - 1].text += rep;
                else tokens.push(new Token(rep));
                text = txt;

            } else if (c === "#") {
                const [t, txt]= MdParser._heading(text);
                tokens.push(t);
                text = txt;

            } else if (c === '\`') {
                const [t, txt] = MdParser._code(text);
                tokens.push(t);
                text = txt;

            } else if (MdConst.STYLE_CHARS.includes(c)) {
                const [tkns, txt] = MdParser._style(text);
                tokens.push(...tkns);
                text = txt;

            } else if (c === '[') {
                const [t, txt] = MdParser._link(text);
                tokens.push(t);
                text = txt;

            } else {
                const [part, txt] = MdParser.consume(text, MdConst.SPECIAL_CHARS)
                tokens.push(new Token(part));
                text = txt;
            }
        }
        // console.log(tokens)
        return tokens;
    }

    /**
     * LINK
     */

    static _link(text: string): [Token, string] {
        try {
            const [linkTxt, txt] = MdParser.consumeTo(text, ']');

            if (txt.charAt(0) === '(') {
                const [linkUrl, txt2] = MdParser.consumeTo(txt, ')');

                const t = new Token(linkTxt);
                t.type |= Token.LINK;
                t.data = linkUrl;

                return [t, txt2];

            } else {    // no link return normal text
                return [new Token(`[${linkTxt}]`), txt];
            }
        } catch (e) {
            const t = text.slice(0, 1);
            const txt = text.slice(1);
            return [new Token(t), txt];
        }


    }

    /**
     * STYLES
     */

    static _getToken(text: string, ref: string[]=MdConst.STYLE_CHARS) {
        // console.log(text);
        let tkn = ''
        let n = 0;
        while(ref.includes(text.charAt(n))) {
            tkn += text.charAt(n);
            n++;
        }
        return [tkn, tkn.split('').reverse().join('')];
    }

    static _maskByToken(temp: string, mask: number) {
        if (temp === '*') {
            mask |= Token.ITALIC;
        } else if (temp === '**') {
            mask |= Token.BOLD;
        } else if (temp === '***') {
            mask |= Token.BOLD | Token.ITALIC;
        } else if (temp === '__') {
            mask |= Token.UNDERLINE;
        } else if (temp === '~~') {
            mask |= Token.STRIKETHROUGH;
        }
        return mask;
    }

    static _getTokenMask(tkn: string) {
        let mask = 0x0;
        let temp = '';

        const n = 0;

        while (tkn != '') {
            if (tkn.charAt(0) != temp.charAt(0)) {
                // evaluate token mask
                mask = MdParser._maskByToken(temp, mask);

                // update temp
                temp = tkn.charAt(0)
            } else {
                temp += tkn.charAt(0);
            }
            tkn = tkn.slice(1);
        }
        mask = MdParser._maskByToken(temp, mask);
        return mask;
    }

    static _style(text: string): [Token[], string] {
        let tokens: Token[] = [];

        const [tkn, rtkn] = MdParser._getToken(text);

        try {
            const [part, txt] = MdParser.consumeTo(text, rtkn, tkn.length);

            // console.log(tkn, part, rtkn);
            const mask = MdParser._getTokenMask(tkn);
            const ts = MdParser.parse(part).map(token => {
                token.type |= mask;
                return token;
            });
            tokens.push(...ts);

            return [tokens, txt];
        } catch (e) {
            const [part, txt] = MdParser._errorConsume(text, tkn.length);
            tokens.push(new Token(part));

            return [tokens, txt];
        }
    }

    /**
     * CODE
     * */

    static _code(text: string): [Token, string] {
        let n = 0;
        while (text.charAt(n) == '\`') {
            n++;
        }

        const t = new Token('');
        let tkn = '`';

        try {
            if (n === 1) {
                t.type |= Token.INLINE_CODE;
            } else if (n === 3) {
                tkn = '```';
                t.type |= Token.CODE_BLOCK
            }

            const [prt, txt] = MdParser.consumeTo(text, tkn, n);
            t.text = prt;
            return [t, txt];
        } catch (e) {
            const [prt, txt] = MdParser._errorConsume(text, n);
            t.text = prt;
            return [t, txt];
        }
    }

    /**
     * HEADING
     * */

    static _heading(text: string): [Token, string] {
        let n = 0;
        while (text.charAt(n) == '#') {
            n++;
        }

        let part;
        let txt;

        try {
            const [p, t] = MdParser.consumeTo(text, '\n', n, 1);
            part = p;
            txt = t;
        } catch (e) {
            const [p, t] = MdParser._errorConsume(text, n);
            part = p;
            txt = t;
        }
        const t = new Token(part.trimLeft());
        t.type |= Token.HEADING;
        t.data = n;
        return [t, txt];

        // TODO: Add support for text styling within header
    }

    /**
     * ESCAPE
     * */

    static _escape(text: string): [string, string] {
        const c1 = text.charAt(1);
        const n = MdConst.ESCAPE_KEYS.indexOf(c1);

        let rep = c1;
        if ( n >= 0) rep = MdConst.ESCAPE_REPLACEMENTS[n];

        text = text.slice(2);
        return [rep, text];
    }
}