import {Text} from "./text";
import {MdParser, Token} from "./mdParser";
import {Line, LineParser, TextContext} from "./lineParser";
import {TokenComponents} from "./tokenComponents";
import {Link, linkHandler, LinkHandler} from "../linkHandler";
import {stateMachine} from "../../logic/stateMachine";
import {SurfaceObject} from "../../canvas/surfaceObject";
import {surface} from "../../canvas/surface";
import {MdCodeBlockObj} from "./mdCodeBlock";
import {SurfaceConst} from "../../canvas/surfaceConst";


class MdCodeBlock {
    count: number = 0;
    p0 = [0, 0];
    p1 = [0, 0];

    reset() {
        this.count = 0;
        this.p0 = [0, 0];
        this.p1 = [0, 0];
    }
}


export class MdText extends Text {

    tokens: Token[] = [];
    textContext: TextContext = new TextContext();

    icb: MdCodeBlock = new MdCodeBlock();
    cb: MdCodeBlock = new MdCodeBlock();
    lb: MdCodeBlock = new MdCodeBlock();

    hint = "Markdown Text";

    links: Link[] = [];

    codeBlocks: MdCodeBlockObj[] = [];

    processText(ctx: CanvasRenderingContext2D, text: string, ox: number, oy: number, s: number) {
        this.tokens = MdParser.parse(this.text);
        this.textContext = LineParser.splitTokens(ctx, this.tokens, this.lineHeight);
        this.renderText = LineParser.parseLines(ctx, this.textContext, this.rect[2], this.lineHeight);
        // this.linkHandler.trackingStart();
        // LineParser.parse(ctx, this.tokens, this.rect[2], this.lineHeight);
    }

    processTextOnWidthChanged(ctx: CanvasRenderingContext2D, text: string, ox: number, oy: number, s: number) {
        this.renderText = LineParser.parseLines(ctx, this.textContext, this.rect[2], this.lineHeight);
        // this.linkHandler.trackingStart();
    }

    _renderEdit(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number): number {
        linkHandler.removeFrom(this.id)
        return super._renderEdit(ctx, ox, oy, s);
    }

    _getNextToken(lines: Line[], i:number, j:number) {
        if (j < lines[i].tokens.length-1) {
            return {next: lines[i].tokens[j+1], i: i};
        } else {
            if (i < lines.length-1) {
                return {next: lines[i + 1].tokens[0], i: i + 1};
            } else {
                return {next: lines[i].tokens[j], i: i};
            }
        }
    }

    _renderText(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number): number {
        this._styleContext(ctx, s);
        ctx.fillStyle = this.color;

        this.links.length = 0;
        const lines = this.renderText as Line[];
        let off = [0, 0];
        let postOff = [0, 0];

        let codeBlockCount = 0;

        for (let i = 0; i < lines.length; i++) {
            const line = lines[i];
            off[0] = 0;

            if (line.tokens.length && (line.tokens[0].type & Token.CODE_BLOCK)) {
                const p = i - 1 < 0 ? 0 : i - 1;
                const n = i + 1 >= lines.length ? lines.length - 1 : i + 1;
                const prev = lines[p];
                const next = lines[n];

                if (prev.tokens.length && !(prev.tokens[0].type & Token.CODE_BLOCK)) {
                    // Add new MdCodeBlock
                    codeBlockCount++;
                    if (this.codeBlocks.length < codeBlockCount) {
                        // add new MdCodeBlock
                        const cb = new MdCodeBlockObj(this);
                        cb.updatePos(off[0], off[1], this.rect[2], this.lineHeight)
                        this.codeBlocks.push(cb);
                    } else {
                        this.codeBlocks[codeBlockCount-1].reset();
                        this.codeBlocks[codeBlockCount-1].updatePos(off[0], off[1], this.rect[2], this.lineHeight)
                    }

                }

                const cbo = this.codeBlocks[codeBlockCount-1];
                cbo.push(line);

                if (!next.tokens.length || (next.tokens.length && !(next.tokens[0].type & Token.CODE_BLOCK))) {
                    cbo.draw(ctx, ox + this.rect[0], oy + this.rect[1], s);
                    off[1] += 5;
                }
            }
            else {
                for (let j = 0; j < line.tokens.length; j++) {
                    const t = line.tokens[j];
                    const o = this._getNextToken(lines, i, j);

                    LineParser._styleContext(ctx, t, this.lineHeight, s);
                    ctx.fillStyle = this.color;

                    // if (t.type & Token.CODE_BLOCK) {
                    //     const mod = this._handleCodeBlock(ctx, off, t, o.next,
                    //         [i, o.i], j, line, ox, oy, s);
                    //     off = mod.off;
                    //     postOff = mod.postOff;
                    //
                    // } else
                    if (t.type & Token.INLINE_CODE) {
                        const mod = this._handleInlineCode(ctx, off, t, o.next,
                            [i, o.i], j, line, ox, oy, s);
                        off = mod.off;
                        postOff = mod.postOff;

                        ctx.fillStyle = '#953131';
                    } else if (t.type & Token.LINK) {
                        const mod = this._handleLink(ctx, off, t, o.next,
                            [i, o.i], j, line, ox, oy, s);
                        off = mod.off;
                        postOff = mod.postOff;
                        // off = this._handleInlineCode(ctx, off, t, j, line, ox, oy, s);
                        ctx.fillStyle = '#2076af';
                        // t.type |= Token.UNDERLINE;
                    }

                    if (t.type & Token.UNDERLINE) {
                        this._handleUnderline(ctx, off, t, j, line, ox, oy, s);
                    }

                    if (t.type & Token.STRIKETHROUGH) {
                        this._handleStrikeThrough(ctx, off, t, j, line, ox, oy, s);
                    }

                    ctx.fillText(t.text,
                        (ox + this.rect[0] + off[0]) * s,
                        (oy + this.rect[1] + off[1] + line.height) * s);

                    off[0] += line.widths[j]

                    // Post Off
                    off[0] += postOff[0];
                    off[1] += postOff[1];
                    postOff = [0, 0];
                }
            }
            off[1] += line.height;
        }
        if (codeBlockCount < this.codeBlocks.length) {
            this.codeBlocks.length = codeBlockCount;
        }
        linkHandler.updateLinks(this.id, this.links);
        return off[1] + this.fontSize;
    }

    _handleLink(ctx: CanvasRenderingContext2D,
                off: number[], t: Token, next: Token,
                i:number[], j: number, line: Line,
                ox: number, oy: number, s: number) {
        let postOff = [0, 0];
        if (this.lb.count == 0) {
            off[0] += 2.5;
            this.lb.p0 = [
                ox + this.rect[0] + off[0],
                oy + this.rect[1] + off[1] + line.height * 0.15
            ]
            off[0] += 2.5;
        }
        this.lb.count++;

        if ((next.type & Token.LINK && next.data != t.data) ||      // different link concurrently
            !(next.type & Token.LINK)                               // next is not a link
            || i[1] > i[0]                                          // new line (split link rect draw)
            || next == t) {                                         // no more next
            const x1 = ox + this.rect[0] + off[0] + line.widths[j];
            const w = x1 - this.lb.p0[0] + 2.5;

            ctx.save();
            ctx.strokeStyle = "#2076af";
            ctx.lineWidth = s;
            TokenComponents._pathRoundRect(ctx, this.lb.p0[0], this.lb.p0[1], w, line.height, 6, s);
            ctx.stroke();
            ctx.restore();

            if ((next.type & Token.LINK && next.data != t.data) ||      // different link concurrently
                !(next.type & Token.LINK) || next == t) {
                const [offx, offy] = surface.getOffset();
                const lnk = new Link();
                lnk.rect = [
                    this.lb.p0[0] - offx,   this.lb.p0[1] - offy,
                    w,                      line.height
                ];
                lnk.url = t.data as string;
                this.links.push(lnk)
            }

            this.lb.reset();
            postOff[0] += 5;
        }

        return {off, postOff};
    }

    _handleCodeBlock(ctx: CanvasRenderingContext2D,
                     off: number[], t: Token, next: Token,
                     i:number[], j: number, line: Line,
                     ox: number, oy: number, s: number) {
        let postOff = [0, 0];

        if (this.cb.count == 0) {
            off[1] += 20;
            this.cb.p0 = [
                ox + this.rect[0] + off[0],
                oy + this.rect[1] + off[1]
            ];
        }
        this.cb.count++;

        if (j == 0) off[0] += 20;

        if (!next || !(next.type & Token.CODE_BLOCK) || next == t) {
            const w = this.rect[2];
            const y1 = oy + this.rect[1] + off[1] + line.height;
            const h = y1 - this.cb.p0[1] + 20;

            ctx.save();
            ctx.strokeStyle = ctx.fillStyle + '55';
            ctx.lineWidth = s;
            TokenComponents._pathRoundRect(ctx, this.cb.p0[0], this.cb.p0[1], w, h, 6, s);
            ctx.stroke();
            ctx.restore();

            this.cb.reset();

            postOff[1] += 20;
        }

        return {off, postOff};
    }

    _handleInlineCode(ctx: CanvasRenderingContext2D,
                      off: number[], t: Token, next: Token,
                      i:number[], j: number, line: Line,
                      ox: number, oy: number, s: number) {

        let postOff = [0, 0];
        if (this.icb.count == 0) {
            off[0] += 2.5;
            this.icb.p0 = [
                ox + this.rect[0] + off[0],
                oy + this.rect[1] + off[1] + line.height * 0.15
            ]
            off[0] += 2.5;
        }
        this.icb.count++;

        if (!(next.type & Token.INLINE_CODE) || i[1] > i[0] || next == t) {
            const x1 = ox + this.rect[0] + off[0] + line.widths[j];
            const w = x1 - this.icb.p0[0] + 2.5;

            ctx.save();
            ctx.strokeStyle = ctx.fillStyle + '55';
            ctx.lineWidth = s;
            TokenComponents._pathRoundRect(ctx, this.icb.p0[0], this.icb.p0[1], w, line.height, 6, s);
            ctx.stroke();
            ctx.restore();

            this.icb.reset();

            postOff[0] += 5;
        }

        return {off, postOff};
    }

    _handleUnderline(ctx: CanvasRenderingContext2D,
                     off: number[], t: Token, j: number, line: Line,
                     ox: number, oy: number, s: number) {
        const x = ox + this.rect[0] + off[0];
        const y = oy + this.rect[1] + off[1] + line.height * 0.95;
        const x1 = x + line.widths[j];

        ctx.save();
        ctx.lineWidth = 2 * s;
        ctx.strokeStyle = ctx.fillStyle;
        ctx.beginPath();
        ctx.moveTo(x * s, y * s);
        ctx.lineTo(x1 * s, y * s);
        ctx.stroke();
        ctx.restore();
    }

    _handleStrikeThrough(ctx: CanvasRenderingContext2D,
                         off: number[], t: Token, j: number, line: Line,
                         ox: number, oy: number, s: number) {
        const x = ox + this.rect[0] + off[0] -2;
        const y = oy + this.rect[1] + off[1] + line.height * 0.7;
        const x1 = x + line.widths[j] + 4;

        ctx.save();
        ctx.lineWidth = 2 * s;
        ctx.strokeStyle = ctx.fillStyle;
        ctx.beginPath();
        ctx.moveTo(x * s, y * s);
        ctx.lineTo(x1 * s, y * s);
        ctx.stroke();
        ctx.restore();
    }

    onMouseMove(dxy: number[], ox: number, oy: number, s: number, evt: MouseEvent) {
        // if (!this.linkHandler.onHover(ox + this.rect[0], oy + this.rect[1], s, evt))
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT / s, evt.pageY * SurfaceConst.FIDEL_MULT / s];
        let found = false;
        for (let i = 0; i < this.codeBlocks.length; i++) {
            const cb = this.codeBlocks[i];
            const [x0, y0] = [cb.rect[0] + ox + this.rect[0], oy + cb.rect[1] + this.rect[1]];
            const [x1, y1] = [x0 + cb.rect[2], y0 + cb.rect[3]];

            if (x >= x0 && x <= x1 && y >= y0 && y <= y1) {
                surface.updateHover(cb);
                found = true;
                break;
            }
        }
        if (!found) super.onMouseMove(dxy, ox, oy, s, evt);
    }

    onClick(mId: number, ox: number, oy: number, s: number, evt: MouseEvent) {
        // if (!this.linkHandler.onClick(mId, ox + this.rect[0], oy + this.rect[1], s, evt))
            super.onClick(mId, ox, oy, s, evt);
    }

    _pannableCheck(dxy:number[], x: number, y: number, ox: number, oy: number, s: number): boolean {
        if (this.codeBlocks.length > 0) {
            for(let i = 0; i < this.codeBlocks.length; i++) {
                const cb = this.codeBlocks[i];
                if (cb.pannable) {
                    return cb.setOffset(dxy);
                }
            }
        }
        return false;
    }
}