

export class TokenComponents {
    static _drawQuoteTab(ctx: CanvasRenderingContext2D,
                         s:number, xy0:number[], xy1:number[],
                         color: string='#555', w: number=5) {
        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.lineWidth = w;
        const x = (xy0[0] + w/2) * s;
        ctx.moveTo(
            x,
            (xy0[1]) * s);
        ctx.lineTo(x, (xy1[1]) * s)
        ctx.stroke();
        ctx.restore();
    }

    static _pathRoundRect(ctx: CanvasRenderingContext2D, x:number, y:number, w:number, h:number, r:number, s:number) {
        ctx.beginPath();
        ctx.moveTo( x * s, (y + r) * s);
        ctx.arcTo(x * s, y * s, (x+w) * s, y * s, r * s);
        ctx.arcTo((x+w) * s, y * s, (x+w) * s, (y+h) * s, r * s);
        ctx.arcTo((x+w) * s, (y+h) * s, x * s, (y+h) * s,  r * s);
        ctx.arcTo(x * s, (y+h) * s,  x * s, (y + r) * s, r * s);
        ctx.lineTo(x * s, (y + r) * s);
    }
}