import {SurfaceObject} from "../../canvas/surfaceObject";
import {Token} from "./mdParser";
import {Line, LineParser} from "./lineParser";
import {MdText} from "./mdText";
import {Shapes} from "../../canvas/shapes";
import {TokenComponents} from "./tokenComponents";


export class MdCodeBlockObj {
    parent: MdText;

    pannable: boolean = false;

    rect:number[] = [0, 0, 0, 0]
    offset = [0, 0];
    internal = [0, 0];

    lineHeight = 1.5;
    totalHeight = 0;
    lines: Line[] = [];

    constructor(parent: MdText) {
        this.parent = parent;
    }

    updatePos(x:number, y:number, w:number, lineHeight:number) {
        this.rect[0] = x;
        this.rect[1] = y;
        this.rect[2] = w;

        this.lineHeight = lineHeight;
    }

    reset() {
        this.totalHeight = 0;
        this.lines.length = 0;
    }

    push(line: Line) {
        this.lines.push(line);
        this.totalHeight += line.height;
        this.rect[3] = this.totalHeight + 20;
    }

    setOffset(dxy:number[]) {
        if (Math.abs(dxy[0]) > Math.abs(dxy[1]) ) {
            if (this.internal[0] > this.rect[2]) {
                const nval = this.offset[0] + dxy[0];
                const max_delta = this.internal[0] - this.rect[2] + 20;
                if (nval <= -max_delta) {
                    this.offset[0] = -max_delta;
                } else if (nval >= 0) {
                    this.offset[0] = 0;
                } else {
                    this.offset[0] = nval;
                }
                return true;
            }
        }
        return false;
    }

    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, lim?: number) {
        let off = [20, 15];

        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle = "#fff";
        ctx.fillStyle = "#333";
        TokenComponents._pathRoundRect(ctx,
            ox + this.rect[0], oy + this.rect[1] + 10,
            this.rect[2], this.rect[3], 5, s);
        ctx.fill();
        ctx.stroke();
        ctx.clip();
        // ctx.restore();

        for (let i = 0; i < this.lines.length; i++) {
            const line = this.lines[i];
            off[0] = 20;

            for (let j = 0; j < line.tokens.length; j++) {
                const t = line.tokens[j];

                if (off[0] + this.offset[0] > this.rect[2]) break;

                LineParser._styleContext(ctx, t, this.lineHeight, s);
                ctx.fillStyle = "#e5e5e5";
                ctx.fillText(t.text,
                    (ox + off[0] + this.rect[0] + this.offset[0]) * s,
                    (oy + off[1] + this.rect[1] + line.height + this.offset[1]) * s);

                off[0] += line.widths[j];
            }
            if (off[0] >  this.internal[0]) this.internal[0] = off[0];
            off[1] += line.height;
        }
        ctx.closePath();
        ctx.restore();

        this.internal[1] = off[1];
    }


}