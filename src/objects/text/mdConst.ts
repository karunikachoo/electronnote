export class MdConst {
    static readonly H1 = 48
    static readonly H2 = 40
    static readonly H3 = 32
    static readonly NORMAL = 24
    static readonly FONT_SIZE = [MdConst.NORMAL, MdConst.H1, MdConst.H2, MdConst.H3];

    static readonly SPECIAL_CHARS = [
        '\\',   // Escape Characters
        '`',    // Code Blocks
        '*',    // Italic or Bold or both
        '_',    // Underline
        '~',    // Strikethrough
        '#',    // Titles
        '['     // Links (have to do additional checks)
    ];

    static readonly EDITOR_CHARS = [
        '`', '*', '_'
    ]

    static readonly STYLE_CHARS = ['*', '_', '~'];
    static readonly VALID_TOKENS = ['***', '**', '*', '__', '~~'];

    static readonly ESCAPE_KEYS = ['n'];
    static readonly ESCAPE_REPLACEMENTS = ['\n'];

    static readonly NORMAL_FONT = "Roboto";
    static readonly MONO_FONT = "RobotoMono";

    static readonly TTSTING = "test line\n" +
        "This line has an `inline code block`.\n" +
        "Let's test multiline code blocks\n" +
        "```type\n" +
        "code code code\n" +
        "    text = \"sdkanlskdn\";\n" +
        "code code```\n\n" +
        "## Title 1\n";

    static readonly TEST_STRING = "test line\n" +
        "This line has an `inline code block`.\n" +
        "Let's test multiline code blocks\n" +
        "```type\n" +
        "code code code\n" +
        "code 2 code 2 code 2\n" +
        "code code```\n\n" +
        "## Title 1\n" +
        "This line has __**~~mixed text~~**__\n" +
        "This **has an *embedded* style**.\n" +
        "This **has an ~~Embedded~~ style**.\n" +
        "\n\nThis **line has ~~multiple~~ *embedded* styles**.\n" +
        "This line has a **bold text**\n" +
        "This line has an *italic text*.\n" +
        "This has a **cross-line \n" +
        "bold text**. Does it work?\n\n" +
        "# Title 2 *bold* text\n" +
        "This section has a list\n" +
        "- Should we even handle lists?\n" +
        "- They should be fine as is. I usually don't like the formatting anyways\n" +
        "- Maybe I should also set the font to monospace?\n\n" +
        "### Title3\n" +
        "thus is [test link]($id) \n" +
        "This section tests the length of texts. If a line is too long, is the compiler able to handle it? Does it fail stupidly?\n" +
        "What is the performance impact of doing sth like this?\n" +
        "    Let's test for trailing styles \n\n" +
        "### Title4\n" +
        "Test trailing **__style";

    static readonly TEST_CODE = "==>[WARN] (mali_base_csf_u) BASE: In file: ../product/user/midgard/csf/mali_kbase_csf.c  line: 562 kbase_csf_queue_kick\n" +
        "queue not bound\n" +
        "Pass: S:{base_csf_gpu_queue_bind} T:{gpu_queue_doorbell_access [0x0004]} F:{ [0x00]} D:{1} - (implicit pass)\n" +
        "...\n" +
        "Initializing: S:{base_csf_gpu_queue_bind} T:{gpu_queue_active_bit_check [0x0007]} D:{0}\n" +
        "Running: S:{base_csf_gpu_queue_bind} T:{gpu_queue_active_bit_check [0x0007]} D:{0}\n" +
        "Fail: S:{base_csf_gpu_queue_bind} T:{gpu_queue_active_bit_check [0x0007]} F:{ [0x00]} D:{10001} - active_mask==expected_mask fail [0==255] (../product/base/tests/internal/unit_tests/csf/mali_base_suite_csf_gpu_queue_bind.c:640) All groups are not yet active\n" +
        "new_active_mask 0\n" +
        "...\n" +
        "new_active_mask 0\n" +
        "Fail: S:{base_csf_gpu_queue_bind} T:{gpu_queue_active_bit_check [0x0007]} F:{ [0x00]} D:{10003} - loop_count<num_iterations fail [200<200] (../product/base/tests/internal/unit_tests/csf/mali_base_suite_csf_gpu_queue_bind.c:661) active groups mask remained same as 0 ";
}