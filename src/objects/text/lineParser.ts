import {Token} from "./mdParser";
import {MdConst} from "./mdConst";

export class Line {
    tokens: Token[] = [];
    height: number = 0;
    width: number = 0;
    widths: number[] = [];
}

export class TextContext {
    tokens: Token[] = [];
    heights: number[] = [];
    widths: number[] = [];
}


export class LineParser {

    /**
     * HEAVY PROCESSING
     */

    static readonly SPECIAL_CHAR = ['\n'];

    static _buildLine(ctx: CanvasRenderingContext2D, tokens: Token[],
                      w:number, lineHeight: number): [Line, Token[]] {
        let line = new Line();

        let iter = 0;
        while (iter < 10000 && tokens.length > 0) {
            let t = tokens.splice(0, 1)[0];

            if (t.text == '' && !(t.type & Token.RETURN)) continue;

            const parts = t.text.split('\n');
            // console.log(t.text, ':', parts, line);

            // break into sentences with returns
            if (parts.length > 1) {     // split into sections, push the rest into the queue.
                const partTokens = parts.map((txt, i) => {
                    const partTkn = t.copy();
                    partTkn.text = txt;
                    if (i != parts.length-1) partTkn.type |= Token.RETURN;
                    // partTkn.type |= Token.RETURN;
                    return partTkn;
                })

                t = partTokens.splice(0, 1)[0];
                tokens = [...partTokens, ...tokens];
            }

            // console.log(t);

            const height = this._styleContext(ctx, t, lineHeight);
            let width = ctx.measureText(t.text).width;

            // break into words (the smallest size);
            if (line.width + width > w) {
                // we could spit it and run the thing again;
                const words = t.text.split(/(\s+)/g);

                // parse words into tokens
                const wordTokens = words.map((txt, i) => {
                    const wordTkn = t.copy();
                    wordTkn.text = txt;
                    // clear return flag
                    wordTkn.type &= ~Token.RETURN;
                    return wordTkn;
                })

                // if the line originally had a return set return flag to the last word
                if (t.type & Token.RETURN) wordTokens[wordTokens.length-1].type |= Token.RETURN;

                // grab first word token
                t = wordTokens.splice(0, 1)[0];
                // Push remainder to token list
                tokens = [...wordTokens, ...tokens];
            }

            width = ctx.measureText(t.text).width;

            // doesn't fit into line, pushing to tokens.
            if (line.width + width > w) {
                tokens = [t, ...tokens];
                break;
            } else {
                line.tokens.push(t);
                if (height > line.height) line.height = height;
                line.width += width;
                line.widths.push(width);

                if (t.type & Token.RETURN) break;
            }

            iter++;
        }
        return [line, tokens];
    }

    static parse(ctx: CanvasRenderingContext2D, tkns: Token[],
                 w: number, lineHeight: number) {
        let lines = [];
        let count = 0;
        let tokens = [...tkns];

        while (tokens.length > 0 && count < 200000) {
            const [l, tkns] = LineParser._buildLine(ctx, tokens, w, lineHeight);
            lines.push(l);
            tokens = tkns;
            count++;
        }

        // console.log(lines);

        return lines;
    }

    static _styleContext(ctx: CanvasRenderingContext2D, t: Token,
                  lineHeight: number, s: number=1) {
        const style = (t.type & Token.ITALIC) ? 'italic' : 'normal';
        const weight = (t.type & Token.BOLD) ? 600 : 200;
        const size = (t.type & Token.HEADING) ? MdConst.FONT_SIZE[t.data as number] : MdConst.NORMAL;
        const font = (t.type & Token.CODE_BLOCK) || (t.type & Token.INLINE_CODE) ? "RobotoMono": "Roboto";
        const height = size * lineHeight * s;

        ctx.font = `${style} ${weight} ${size * s}px ${font}`;
        return height;
    }

    /**
     * SMART PROCESSING: ONLY RE-PARSE TOKEN WIDTHS ON HASH CHANGE
     * FOR RESIZE REUSE CALCULATED WIDTHS.
     * - basically introduce another step in the parsing process
     */

    static _splitLineBreaks(t: Token) {
        let parts = t.text.split('\n');

        // parts = parts.filter(Boolean);
        let partTokens = parts.map((txt, ind) => {
            const token = t.copy();
            token.text = txt;
            if (ind != parts.length-1) token.type |= Token.RETURN;
            return token;
        })

        partTokens = partTokens.filter((token) => {
            return !(!(token.type & Token.RETURN) && token.text == "")
        })

        return partTokens;
    }

    static _splitWords(ctx: CanvasRenderingContext2D, tokens: Token[], lineHeight: number) {
        let ret: Token[] = [];
        let widths: number[] = [];
        let heights: number[] = [];

        for (let n = 0; n < tokens.length; n++) {
            let partToken = tokens[n];

            if (!((partToken.type & Token.RETURN) && partToken.text == "")) {

                let words = partToken.text.split(/(\S+\s|\s+)/g).filter(Boolean);

                const h = LineParser._styleContext(ctx, partToken, lineHeight);

                let wordTokens = words.map((txt, ind) => {
                    const wordToken = partToken.copy();
                    wordToken.text = txt;
                    wordToken.type &= ~Token.RETURN;

                    widths.push(ctx.measureText(txt).width);
                    heights.push(h);

                    return wordToken;
                } )

                // Set return flag for last word
                if (partToken.type & Token.RETURN) wordTokens[wordTokens.length - 1].type |= Token.RETURN

                ret.push(...wordTokens);
            } else {
                widths.push(0);
                heights.push(LineParser._styleContext(ctx, partToken, lineHeight));
                ret.push(partToken);
            }
        }
        return {ret, widths, heights};
    }

    static splitTokens(ctx: CanvasRenderingContext2D, tokens: Token[], lineHeight: number) {
        const txtCtx = new TextContext();
        for (let i = 0; i < tokens.length; i++) {
            let t = tokens[i];

            const partTokens = this._splitLineBreaks(t);

            // console.log(partTokens);

            const words = LineParser._splitWords(ctx, partTokens, lineHeight);
            txtCtx.tokens.push(...words.ret);
            txtCtx.widths.push(...words.widths);
            txtCtx.heights.push(...words.heights);

            // console.log(txtCtx);
            // console.log("\n\n\n\n");
        }
        // console.log(tokens)
        // console.log(txtCtx);

        return txtCtx;
    }

    static parseLines(ctx: CanvasRenderingContext2D,
                      textContext: TextContext, w: number, lh: number) {
        const lines: Line[] = [];

        const tokens = [...textContext.tokens];
        const widths = [...textContext.widths];
        const heights = [...textContext.heights];

        // console.log(textContext);

        let line = new Line();
        while (tokens.length > 0) {
            const t = tokens.shift();
            const tw = widths.shift();
            const th = heights.shift();

            if (t.type & Token.CODE_BLOCK) {

            } else {
                if (tw > w) {
                    let txt = "";
                    LineParser._styleContext(ctx, t, lh);
                    while (ctx.measureText(txt + t.text.charAt(txt.length)).width < w - line.width) {
                        txt += t.text.charAt(txt.length);
                    }
                    const t0 = t.copy();
                    t0.text = txt;
                    t.text = t.text.slice(txt.length);

                    line.tokens.push(t0);
                    line.width += tw;
                    line.widths.push(ctx.measureText(txt).width);

                    lines.push(line);
                    line = new Line();
                }

                if (line.width + tw > w) {
                    lines.push(line);
                    line = new Line();
                }
            }

            line.tokens.push(t);
            line.width += tw;
            line.widths.push(tw);


            if (th > line.height) line.height = th;

            if (t.type & Token.RETURN) {
                lines.push(line);
                line = new Line();
            }
        }
        lines.push(line);

        // console.log(lines);
        return lines;
    }


    /**
     * STRING LINE PARSING
     */

    static parseText(ctx: CanvasRenderingContext2D, text: string, w:number) {
        // console.log({text});
        // text.replace(/(\s{4})/g, '');
        // console.log({text})
        let words = text.split(/(\s+)/g);

        let lines = [];

        while (words.length > 0) {

            let line = '';

            while (words.length > 0) {
                let word  = words.splice(0, 1)[0];
                const i = word.indexOf('\n');

                if (i >= 0) {
                    // "....\n..." or "....\n" or "\n...." or "..\n...\n..."
                    // "\n...\n.." or "..\n...\n"

                    const nw = word.slice(i+1);
                    word = word.slice(0, i+1);

                    if (nw.length) words = [nw, ...words];
                }

                if (word != '') {
                    // if (word.includes('\t')) word.replace('\t', '    ');
                    if (ctx.measureText(word).width > w) {
                        let letter = word.charAt(0)
                        while(ctx.measureText(line + letter).width < w) {
                            line += letter;
                            word = word.slice(1);
                            letter = word.charAt(0);
                        }
                        words = [word, ...words];
                        break;
                    } else {
                        if (ctx.measureText(line + word).width > w) {
                            words = [word, ...words];
                            break;
                        } else {
                            // const sp = (word.charAt(word.length-1) == '\n') ? '' : ' ';
                            line += word;

                            if (i >= 0) break;
                        }
                    }
                }
            }

            lines.push(line);
        }
        // console.log(lines);
        return lines;
    }
}