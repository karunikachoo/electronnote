import {SurfaceObject} from "../../canvas/surfaceObject";
import {stateMachine} from "../../logic/stateMachine";
import {Line, LineParser} from "./lineParser";
import {SurfaceConst} from "../../canvas/surfaceConst";
import {ddraw} from "../../canvas/dynamicDraw";
import {Utils} from "../../tools/utils";
import {MdConst} from "./mdConst";
import {MultiCaret} from "./caret";
import {clipboard} from "electron";
import {SurfaceObjectKB} from "../../canvas/surfaceObjectKB";
import {Cursor} from "../../canvas/cursor";


export class Text extends SurfaceObjectKB {
    static readonly EDIT_OUTLINE_CL = '#d5780a';

    draggable = false;
    resizeable = false;
    deletable = false;
    editable = true;

    ctx: CanvasRenderingContext2D;
    editing = false;

    caret: MultiCaret = new MultiCaret();

    hint: string = 'Text';
    hash: number = 0;

    widthChanged = false;

    // text:string = MdConst.TEST_STRING;
    text:string = "";

    renderText: string[] | Line[] = [];
    editText: string[] = [];

    fontFamily: string = 'Roboto';
    editFontFamily: string = 'Roboto';
    fontSize:number = 24;
    lineHeight:number = 1.5;
    fontWeight:number = 200;
    color: string = '#333333';

    bottomPadding = 0;

    suffix = "";

    onPrevSibling: (obj: SurfaceObject) => void;
    onNextSibling: (obj: SurfaceObject) => void;
    onObjectRemoveRequest: (obj: SurfaceObject) => void;
    onSaveRequest: (obj: SurfaceObject) => void;
    onEmptyDelete: (obj: SurfaceObject) => void;

    constructor(parent: SurfaceObject, x:number, y:number, w:number) {
        super(parent);

        this.rect[0] = x;
        this.rect[1] = y;
        this.rect[2] = w;
        this.rect[3] = 24 * 1.5;
    }

    _activate() {
        super._activate();
        // stateMachine.addKeyboardListener(this);
        // this.caret = [0, 0];
        // TODO: Move caret to mouse position;
    }

    _deactivate() {
        super._deactivate();
        // stateMachine.removeKeyboardLister(this);
        if (this.editing) {
            this.editing = false;
            this.text = this.editText.join('');
            this.hash++;
        }
        ddraw.event();
    }

    updateWidth(w: number) {
        if (this.rect[2] != w) {
            this.rect[2] = w;
            this.widthChanged = true;
        }
    }

    setSuffix(txt: string) {
        this.suffix = txt;
    }

    /**
     * DRAWING / RENDERING
     */

    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number) {
        if (!this.ctx) this.ctx = ctx;

        const h = Utils.hash(this.text);
        if (h != this.hash) {
            // console.log(this.text)
            if (!this.editing) this.processText(ctx, this.text, ox, oy, s);

            this._styleContext(this.ctx, 1, this.editFontFamily);
            this.editText = LineParser.parseText(ctx, this.text, this.rect[2]);

            // console.log(this.editText);

            this.onChange();
            this.hash = h;
        }

        if (this.widthChanged) {
            this.processTextOnWidthChanged(ctx, this.text, ox, oy, s);
            this.widthChanged = false;
        }

        if (this.editing) this.caret.update(this.editText);

        if (!this.editing) {
            this.outlineColor = SurfaceObject.OUTLINE_CL;

            if (this.editText.length <= 1 && this.editText[0] == '') {
                this.rect[3] = this._renderHint(ctx, ox, oy, s);
            } else {
                this.rect[3] = this._renderText(ctx, ox, oy, s);
            }
        } else {
            this.outlineColor = Text.EDIT_OUTLINE_CL;
            const h = this._renderEdit(ctx, ox, oy, s)
            if (h > this.rect[3]) this.rect[3] = h;
        }

        super.draw(ctx, ox, oy, s);
    }

    processText(ctx: CanvasRenderingContext2D,
                text:string, ox: number, oy: number, s: number) {

        this._styleContext(ctx);
        this.renderText = LineParser.parseText(ctx, this.text, this.rect[2]);
        // Handle token and line processing (potentially different?);
        return;
    }

    processTextOnWidthChanged(ctx: CanvasRenderingContext2D,
                              text:string, ox: number, oy: number, s: number) {
        this._styleContext(ctx);
        this.renderText = LineParser.parseText(ctx, this.text, this.rect[2]);
        return;
    }

    _renderText(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number): number {
        this._styleContext(ctx, s);
        ctx.fillStyle = this.color;

        const height = this.fontSize * this.lineHeight;

        let off = 0;

        const rtext = this.renderText as string[];
        for (let i = 0; i < rtext.length; i++) {
            let txt = rtext[i];

            if (i == rtext.length - 1) txt += this.suffix;

            ctx.fillText(txt,
                (ox + this.rect[0]) * s,
                (oy + this.rect[1] + off + height) * s);

            off += height;
        }

        return off;
    }

    _renderEdit(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number): number {
        this._styleContext(this.ctx, s, this.editing ? this.editFontFamily: this.fontFamily);
        ctx.fillStyle = this.color;

        const height = this.fontSize * this.lineHeight;

        let off = 0;

        for (let i = 0; i < this.editText.length; i++) {
            const x = (ox + this.rect[0]) * s;
            const y0 = (oy + this.rect[1] + off) * s;
            const y = (oy + this.rect[1] + off + height) * s;
            const txt = this.editText[i];

            if (this.caret.rowBounded(i)) {
                const [r0, c0, r1, c1] = this.caret.get();
                if (this.caret.sameRow()) {

                    const start = txt.slice(0, c0);
                    const middl = txt.slice(c0, c1);
                    const end = txt.slice(c1);

                    const w0 = ctx.measureText(start).width;
                    const w1 = ctx.measureText(middl).width;

                    ctx.save()
                    ctx.beginPath();
                    ctx.fillStyle = this.color;
                    ctx.fillRect(x + w0 + 1, y0, w1 + 1, height * s);
                    ctx.restore();

                    ctx.beginPath();
                    ctx.fillText(start, x, y);
                    ctx.save();
                    ctx.fillStyle = "#fff";
                    ctx.fillText(middl, x + w0 + 1, y);
                    ctx.restore();
                    ctx.fillText(end, x + w0 + w1 + 2, y);
                } else if (i == r0) {
                    const start = txt.slice(0, c0);
                    const end = txt.slice(c0);

                    const w0 = ctx.measureText(start).width;
                    const w1 = ctx.measureText(end).width;

                    ctx.save();
                    ctx.beginPath();
                    ctx.fillStyle = this.color;
                    ctx.fillRect(x + w0 + 1, y0, w1 + 1, height * s);
                    ctx.restore();

                    ctx.beginPath();
                    ctx.fillText(start, x, y);
                    ctx.save();
                    ctx.fillStyle = "#fff";
                    ctx.fillText(end, x + w0 + 1, y);
                    ctx.restore();
                } else if (i == r1) {
                    const start = txt.slice(0, c1);
                    const end = txt.slice(c1);

                    const w0 = ctx.measureText(start).width;

                    ctx.save();
                    ctx.beginPath();
                    ctx.fillStyle = this.color;
                    ctx.fillRect(x-1, y0, w0 + 2, height * s);
                    ctx.restore();

                    ctx.save();
                    ctx.beginPath();
                    ctx.fillStyle = "#fff";
                    ctx.fillText(start, x, y)
                    ctx.restore();
                    ctx.fillText(end, x + w0 + 1, y);
                } else {
                    const w0 = ctx.measureText(txt).width;

                    ctx.fillStyle = this.color;
                    ctx.fillRect(x - 1, y0, w0 + 2, height * s);
                    ctx.save();
                    ctx.fillStyle = "#fff";
                    ctx.fillText(txt, x, y);
                    ctx.restore();
                }

            } else {
                ctx.fillText(txt, x, y);
            }
            off += height;
        }
        return off;
    }

    _renderHint(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number): number {
        this._styleContext(ctx, s);
        ctx.fillStyle = this.color + 'aa';

        ctx.fillText(this.hint,
            (ox + this.rect[0]) * s,
            (oy + this.rect[1] + this.fontSize * this.lineHeight) * s);
        return this.fontSize * this.lineHeight + this.bottomPadding;
    }

    _styleContext(ctx: CanvasRenderingContext2D, s:number=1, fontFamily: string=this.fontFamily) {
        ctx.textAlign = 'left';
        // ctx.textBaseline = 'alphabetic';
        ctx.textBaseline = 'bottom';
        ctx.font = `normal ${this.fontWeight} ${this.fontSize * s}px ${fontFamily}`;
    }

    /**
     * TEXT HANDLING
     */

    _updateText() {
        this.text = this.editText.join('');
    }

    _caretMouse(mx:number, my:number) {
        const height = this.fontSize * this.lineHeight;
        const row = Math.floor(my / height);

        const line = this.editText[row];
        let i = 0;
        if (line) {
            this._styleContext(this.ctx, 1, this.editing ? this.editFontFamily : this.fontFamily);

            if (mx < this.ctx.measureText(line).width) {
                while (this.ctx.measureText(line.slice(0, i)).width < mx) {
                    i++;
                }
                return [row, i-1]
            } else {
                i = line.charAt(line.length - 1) == '\n' ? line.length - 1 : line.length;
                return [row, i]
            }
        }
        return [row, i];
    }

    delete() {
        const [r0, c0, r1, c1] = this.caret.get();

        if (this.caret.hasSelection()) {
            // console.error('selection Delete');

            const t0 = this.editText[r0].slice(0, c0);
            const t3 = this.editText[r1].slice(c1);

            this.editText[r0] = t0 + t3;

            const dcount = r1 - r0;
            if (dcount > 0) this.editText.splice(r0 + 1, dcount);

            this.caret.set(r0, c0);
        } else {

            const before = this.editText[r0].slice(0, c0);
            const after = this.editText[r1].slice(c1);

            if (!before.length) {   // [''] | ['...']  should go up one line.
                // console.log('up delete')
                if (r0 > 0) {
                    let txt = this.editText[r0-1];

                    if (txt.charAt(txt.length-1) == '\n') txt = txt.slice(0, txt.length-1);

                    this.editText[r0-1] = txt + after;
                    this.editText.splice(r0, 1);
                    this.caret.set(r0-1, txt.length);
                } else {
                    if (this.onEmptyDelete) this.onEmptyDelete(this);
                }
            } else {
                // console.log(`normal delete: ${before} : ${before.length}`);
                this.editText[r0] = before.slice(0, before.length - 1) + after;
                this.caret.prev(this.editText);
            }
        }

        this._updateText();
    }

    insert(char: string) {
        const [r0, c0, r1, c1] = this.caret.get();

        if (this.caret.hasSelection()) {
            // console.error('selection Insert');

            const before = this.editText[r0].slice(0, c0);
            const after = this.editText[r1].slice(c1);

            this.editText[r0] = before + char + after;

            const dcount = r1 - r0;
            if (dcount > 0) this.editText.splice(r0 + 1, dcount);

            this.caret.set(r0, c0 + char.length);

        } else {
            const before = this.editText[r0].slice(0, c0);
            const after = this.editText[r1].slice(c1);
            this.editText[r0] = before + char + after;
            this.caret.next(this.editText, char.length);
        }

        this._updateText();
    }

    enter() {
        const [r0, c0, r1, c1] = this.caret.get();

        if (this.caret.hasSelection()) {
            // console.error('selection Enter');
            const t0 = this.editText[r0].slice(0, c0);
            const t1 = this.editText[r1].slice(c1);

            this.editText[r0] = t0 + '\n';

            const start = this.editText.slice(0, r0 + 1);
            const end = this.editText.slice(r0 + 1);

            this.editText = [...start, t1, ...end]
            this.caret.set(r0, c0);
            this.caret.next(this.editText, 2);

        } else {
            const before = this.editText[r0].slice(0, c0);
            const after = this.editText[r0].slice(c0);

            // console.log({after});

            this.editText[r0] = before;
            const start = this.editText.slice(0, r0 + 1);
            const end = this.editText.slice(r0 + 1);
            start[start.length-1] += '\n';
            this.editText = [...start, after, ...end];

            this.caret.next(this.editText, 2);
        }

        this._updateText();
    }

    _print() {
        const [r0, c0, r1, c1] = this.caret.get();
        // this.text = this.editText.join('');
        // console.log(this.caret, this.caretSel);
        // console.log(this.caret.c0, this.caret.c1, this.caret.dir);
        const before = this.editText[r0].slice(0, c0);
        const after = this.editText[r0].slice(c0);
        // console.log(this.editText.map(t => {return t.length}));
        // console.log({txt: `${before}|${after}`});
    }

    _copy() {
        // console.log('copy...')
        if (this.caret.hasSelection())
            clipboard.writeText(this.caret.getSelection(this.editText));
    }

    _cut() {
        this._copy();
        if (this.caret.hasSelection()) this.delete();
    }


    _paste() {
        const txt = clipboard.readText();
        this.insert(txt)
    }

    /**
     * INPUT HANDLING
     */

    onKeyPress(e: KeyboardEvent) {
        if (e.key === 'Enter') {
            if (this.editing && !e.metaKey) {
                this.enter()
            }
            this._print();
        } else {
            if (this.editing) this.insert(e.key)
            else if (this.editable) {
                this.editing = true;
                this.caret.jumpTo(this.editText, 1);
                this.insert(e.key);
            }
            this._print();
        }
        ddraw.event();
    }

    onKeyDown(e: KeyboardEvent) {
        if (e.key ==='Escape') {
            if (this.editing) this._deactivate();
        } else if (e.key === 'Backspace') {
            if (this.editing) this.delete();
            else if (this.active && e.metaKey && this.deletable && this.onObjectRemoveRequest) this.onObjectRemoveRequest(this);
        } else if (e.key === 'Tab') {
            if (this.editing) {
                this.insert('    ');
                e.preventDefault();
            }
        } else if ((e.ctrlKey || e.metaKey)) {
            if (e.key == 'Enter' && this.editing) {
                if (e.shiftKey) {
                    if (this.onPrevSibling) this.onPrevSibling(this);
                } else {
                    if (this.onNextSibling) this.onNextSibling(this);
                }
            }
            if (e.key == 's' && !e.shiftKey) {
                if (this.onSaveRequest) this.onSaveRequest(this);
            }
            if (e.key == 'a') {
                e.preventDefault();
                this.caret.selectAll(this.editText);
            }
            if (e.key == 'c') this._copy();
            if (e.key == 'v') this._paste();
            if (e.key == 'x') this._cut();
            if (e.key == 'k') stateMachine.addLink.onClick(null);
            if (e.key == 'ArrowRight') {
                if (e.shiftKey)
                    this.caret.selectJumpTo(this.editText, 1);
                else
                    this.caret.jumpTo(this.editText, 1);
            }
            if (e.key == 'ArrowLeft') {
                if (e.shiftKey)
                    this.caret.selectJumpTo(this.editText, -1);
                else
                    this.caret.jumpTo(this.editText, -1);
            }
        } else if (e.key === 'Home') {
            if (e.shiftKey)
                this.caret.selectJumpTo(this.editText, -1);
            else
                this.caret.jumpTo(this.editText, -1);
        } else if (e.key === 'End') {
            if (e.shiftKey)
                this.caret.selectJumpTo(this.editText, 1);
            else
                this.caret.jumpTo(this.editText, 1);
        } else if (e.key === 'ArrowRight') {
            if (this.editing) {
                if (e.shiftKey)
                    this.caret.selectNext(this.editText)
                else
                    this.caret.next(this.editText);
            }

        } else if (e.key === 'ArrowLeft') {
            if (this.editing) {
                if (e.shiftKey)
                    this.caret.selectPrev(this.editText);
                else
                    this.caret.prev(this.editText);
            }

        } else if (e.key === 'ArrowUp') {
            if (this.editing) {
                if (e.shiftKey)
                    this.caret.selectUp(this.editText);
                else
                    this.caret.up(this.editText);
            }

        } else if (e.key === 'ArrowDown') {
            if (this.editing) {
                if (e.shiftKey)
                    this.caret.selectDown(this.editText);
                else
                    this.caret.down(this.editText);
            }

        }
        this._print();
        ddraw.event();
    }

    onMouseMove(dxy: number[], ox: number, oy: number, s: number, evt: MouseEvent) {

        Cursor.set(Cursor.TEXT);

        super.onMouseMove(dxy, ox, oy, s, evt);
    }

    onClick(mId: number, ox: number, oy: number, s: number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        if (mId == 0) {
            if (this.editing) {
                const [r, c] = this._caretMouse(
                    x / s - ox - this.rect[0],
                    y / s - oy - this.rect[1]);

                if (evt.shiftKey) this.caret.setSelection(this.editText, r, c);
                else this.caret.set(r, c);

                // console.log('click:', this.caret.c0, this.caret.c1);
            }

        }
        super.onClick(mId, ox, oy, s, evt);
    }

    onDrag(dxy: number[], ox: number, oy: number, s: number, evt: MouseEvent) {
        // const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        //
        // if (this.editing) {
        //     const [r, c] = this._caretMouse(
        //         x / s - ox - this.rect[0],
        //         y / s - oy - this.rect[1]);
        //
        //     this.caret.setSelection(r, c);
        // }

        // console.log('drag:', this.caret.c0, this.caret.c1);
        super.onDrag(dxy, ox, oy, s, evt);
    }

    onDoubleClick(mId: number, ox: number, oy: number, s: number, evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        if (!this.editing && this.editable) {
            this._activate();
            this.editing = true;

            const [r, c] = this._caretMouse(
                x / s - ox - this.rect[0],
                y / s - oy - this.rect[1]);

            this.caret.set(r, c);
        } else if (this.editing) {
            this.caret.selectWord(this.editText);
        } else {
            super.onDoubleClick(mId, ox, oy, s, evt);
        }
    }

}