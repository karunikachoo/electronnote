export class MultiCaret {
    c0: Caret = new Caret();
    c1: Caret = new Caret();

    dir: number = 0;
    row: number = 0;

    rowBounded(r:number): boolean {
        return r >= this.c0.r && r <= this.c1.r;
    }

    sameRow(): boolean {
        return this.c0.r == this.c1.r;
    }

    inverted(): boolean {
        if (this.sameRow())
            return this.c0.c > this.c1.c;
        else
            return this.c0.r > this.c1.r;
    }

    swap() {
        const [r, c] = [this.c0.r, this.c0.c];
        this.c0.copy(this.c1);
        this.c1.set(r, c);
    }

    hasSelection(): boolean {
        return !this.sameRow() || this.c0.c != this.c1.c;
    }

    get() {
        return [this.c0.r, this.c0.c, this.c1.r, this.c1.c];
    }

    getCols() {
        return [this.c0.c, this.c1.c];
    }

    getSelection(text: string[]) {
        const [r0, c0, r1, c1] = this.get();

        let lines = '';

        if (this.sameRow()) {
            lines = text[r0].slice(c0, c1);
        } else {
            for (let i = r0; i <= r1; i++) {
                if (i == r0) {
                    lines += text[r0].slice(c0);
                } else if (i == r1) {
                    lines += text[r1].slice(0, c1);
                } else {
                    lines += text[i];
                }
            }

        }
        return lines;
    }

    getSplit(text: string[]) {
        const r0 = text[this.c0.r];
        const r0before = r0.slice(0, this.c0.c);
        const r1 = text[this.c1.r];
        const r1after = r1.slice(this.c1.c);

        let before = "";
        let after = r1after;

        if (this.c0.r > 0) {
            for (let i = 0; i < this.c0.r; i++) {
                before += text[i];
            }
        }
        before += r0before;

        if (this.c1.r < text.length-1) {
            for (let i = this.c1.r + 1; i < text.length; i++) {
                after += text[i];
            }
        }

        return {before, after};
    }

    update(text: string[]) {
        this.c0.update(text);
        this.c1.update(text);
    }

    set(r:number, c:number) {
        this.c0.set(r,c);
        this.c1.set(r,c);
        this.dir = 0;
        this.row = 0;
    }

    setSelection(text:string[], r:number, c:number) {
        this.c1.set(r, c);

        if (this.inverted()) {
            this.swap();
            this.dir = -this.getSelection(text);
        }
    }

    next(text: string[], n: number=1) {
        this.c1.next(text, n);
        this.c0.copy(this.c1);
        this.dir = 0;
        this.row = 0;
    }

    prev(text: string[], n:number=1) {
        this.c0.prev(text, n);
        this.c1.copy(this.c0);
        this.dir = 0;
        this.row = 0;
    }


    down(text: string[], n: number=1) {
        this.c1.down(text, n);
        this.c0.copy(this.c1);
        this.dir = 0;
        this.row = 0;
    }

    up(text: string[], n:number=1) {
        this.c0.up(text, n);
        this.c1.copy(this.c0);
        this.dir = 0;
        this.row = 0;
    }

    jumpTo(text:string[], dir:number) {

        if (dir > 0) {
            const t = text[this.c1.r];
            const w = text[this.c1.r].length;

            this.set(this.c1.r, t.charAt(t.length - 1) == '\n' ? w - 1 : w);
        } else {
            this.set(this.c1.r, 0);
        }
    }

    toEnd(text:string[]) {
        const r = text.length - 1;
        const w = text[r].length;

        this.set(r, w);
    }

    selectNext(text: string[]) {
        if (this.dir >= 0) {
            this.c1.next(text);
        } else if (this.dir < 0) {
            this.c0.next(text);
        }
        this.dir++;
    }

    selectPrev(text: string[]) {
        if (this.dir <= 0) {
            this.c0.prev(text);
        } else if (this.dir > 0) {
            this.c1.prev(text);
        }
        this.dir--;
    }

    selectDown(text: string[]) {
        const s0 = this.getSelection(text);
        if (this.dir >= 0) {
            this.c1.down(text);
            const s1 = this.getSelection(text);
            this.dir += s1.length - s0.length;
        } else if (this.dir < 0) {
            this.c0.down(text);
            const s1 = this.getSelection(text);
            this.dir -= s1.length - s0.length;
        }

        if (this.inverted()) {
            this.swap();
            this.dir = this.getSelection(text).length;
        }
    }

    selectUp(text: string[]) {
        const s0 = this.getSelection(text);
        if (this.dir <= 0) {
            this.c0.up(text);
            const s1 = this.getSelection(text);
            this.dir -= s1.length - s0.length;
        } else if (this.dir > 0) {
            this.c1.up(text);
            const s1 = this.getSelection(text);
            this.dir += s1.length - s0.length;
        }

        if (this.inverted()) {
            this.swap();
            this.dir = -this.getSelection(text).length;
        }
    }

    selectJumpTo(text:string[], dir:number) {
        if (this.dir > 0) {
            const c = this.c1.c;
            const c1 = dir > 0 ? text[this.c1.r].length : 0;

            this.c1.set(this.c1.r, c1);

            if (this.inverted()) {
                this.swap();
                this.dir = -this.getSelection(text).length;
            } else {
                this.dir += c1 - c;
            }
        } else if (this.dir < 0) {
            const c = this.c0.c;
            const c1 = dir > 0 ? text[this.c0.r].length : 0;

            this.c0.set(this.c0.r, c1);

            if (this.inverted()) {
                this.swap();
                this.dir = this.getSelection(text).length;
            } else {
                this.dir += c1 - c;
            }
        } else {
            const c = this.c1.c;
            if (dir > 0) {
                const t = text[this.c1.r];
                const w = text[this.c1.r].length;

                this.c1.set(this.c1.r, t.charAt(t.length-1) == '\n' ? w-1 : w);
                this.dir = this.getSelection(text).length;
            } else {
                this.c0.set(this.c0.r, 0);
                this.dir = -this.getSelection(text).length;
            }
        }
    }

    selectAll(text:string[]) {
        const t = text[text.length-1];

        this.c0.set(0,0);
        this.c1.set(text.length-1, t.charAt(t.length-1) == '\n' ? t.length-1 : t.length);
    }

    selectWord(text:string[]) {
        console.log(this.c0, this.c1)
        const t = text[this.c0.r];
        const parts = t.split(/(\s+|[\.\s\,\?\!\"\'\`])/g);
        let c = 0;
        for (let i = 0; i < parts.length; i++) {
            const s = parts[i];
            if (c + s.length > this.c0.c) {
                this.c0.c = c;
                this.c1.c = c + s.length;
                return;
            } else {
                c += s.length;
            }
        }
    }
}

export class Caret {
    r: number = 0;
    c: number = 0;

    update(text: string[]) {
        if (this.r < text.length) {
            let t = text[this.r];
            if (this.c > t.length) {
                const diff = this.c - t.length;
                this.r++;
                this.c = diff;
            }
        } else {
            while (this.r >= text.length) this.r--;
            this.c = text[this.r].length;

        }
    }

    set(r: number, c: number) {
        this.r = r;
        this.c = c < 0 ? 0 : c;
    }

    get() {
        return [this.r, this.c]
    }

    copy(c: Caret) {
        this.r = c.r;
        this.c = c.c;
    }

    next(text: string[], n: number = 1) {
        this.c += n;
        const w = text[this.r].length;

        if (this.c > w) {
            if (this.r >= text.length - 1) {
                this.c = w;
            } else {
                const diff = this.c - w - 1;
                this.r++;
                this.c = diff;
            }
        }

        return this;
    }

    prev(text: string[], n: number = 1) {
        this.c -= n;

        if (this.c < 0) {
            if (this.r == 0) {
                this.c = 0;
            } else {
                this.r--;
                this.c += text[this.r].length + 1;
            }
        }

        return this;
    }

    up(text: string[], n: number = 1) {
        this.r -= n;

        if (this.r < 0) {
            this.r = 0;
        }

        const w = text[this.r].length;
        if (this.c > w) this.c = w;

        return this;
    }

    down(text: string[], n: number = 1) {
        this.r += n;
        if (this.r >= text.length)
            this.r = text.length - 1;

        const w = text[this.r].length;
        if (this.c > w) this.c = w;

        return this;
    }
}