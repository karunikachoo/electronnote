import {SurfaceObjectKB} from "../canvas/surfaceObjectKB";
import {NativeImage} from "electron";
import {SurfaceObject} from "../canvas/surfaceObject";
import {dialogBox} from "../ui/dialogBox";
import {linkHandler} from "./linkHandler";
import {db} from "../db/db";


export interface SurfImgObj {
    rect: number[]
    img: string
}

export class SurfaceImage extends SurfaceObjectKB {

    retainRatio = true;

    b64Url: string;
    img: HTMLImageElement;
    loaded: boolean = false;

    constructor(parent: SurfaceObject, x:number, y:number) {
        super(parent);

        this.rect[0] = x;
        this.rect[1] = y;
    }

    setB64URL(b64url: string) {
        this.loaded = false;
        this.b64Url = b64url;
        this.img = new Image();
        this.img.src = this.b64Url;
        this.img.onload = () => {this.loaded = true;}
    }

    fromObj(o: Object) {
        const a = o as SurfImgObj;
        this.setB64URL(a.img);
        this.rect = a.rect;
    }

    fromNativeImage(img: NativeImage) {
        this.loaded = false;
        const size = img.getSize();
        this.rect[2] = size.width;
        this.rect[3] = size.height;

        this.b64Url = img.toDataURL();
        this.img = new Image();
        this.img.src = this.b64Url;
        this.img.onload = () => {this.loaded = true;}
    }

    draw(ctx: CanvasRenderingContext2D, x: number, y: number, s: number, lim?: number) {

        if (this.loaded) {
            ctx.drawImage(this.img,
                (x + this.rect[0]) * s, (y + this.rect[1]) * s,
                this.rect[2] * s, this.rect[3] * s)
        }

        super.draw(ctx, x, y, s, lim);
    }

    onKeyDown(e: KeyboardEvent) {
        if (e.key == 'Backspace' && e.metaKey || e.key == 'Delete') {
            this.getParent().removeObject(this);
        }
    }

    toJSON(): Object {
        return {
            rect: [this.rect[0], this.rect[1], this.rect[2], this.rect[3]],
            img: `${this.b64Url}`
        }
    }
}