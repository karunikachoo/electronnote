// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.

import {title} from "./tools/title";
import {stateMachine} from "./logic/stateMachine";
import {ddraw} from "./canvas/dynamicDraw";
import {dialogBox} from "./ui/dialogBox";
import {fsPanel} from "./ui/fsPanel";
import {db} from "./db/db";
import {surface} from "./canvas/surface";
import {zoomSlider} from "./ui/zoomSlider";


function updateSize() {
    surface.updateSize();
    fsPanel.updateSize();
    zoomSlider.updateSize();
}

function init() {
    stateMachine.init('main');

    ddraw.start();

    title.init("title", "title-span");

    // Resize
    document.getElementById('body').onresize = updateSize;

    // UI
    dialogBox.init();
    fsPanel.init();
    zoomSlider.init();

    db.init();

    // stateMachine.addNoteTest();
}

window.addEventListener("DOMContentLoaded", () => {
    init();
});
