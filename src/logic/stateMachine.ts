import {SurfaceMouseHandler} from "../canvas/surfaceMouseHandler";
import {surface} from "../canvas/surface";
import {AddNote} from "../toolbar/addNote";
import {ddraw} from "../canvas/dynamicDraw";
import {SurfaceConst} from "../canvas/surfaceConst";
import {Note} from "../objects/note";
import {Cursor} from "../canvas/cursor";
import {MdConst} from "../objects/text/mdConst";
import {AddLink} from "../toolbar/addLink";
import {SaveNote} from "../toolbar/saveNote";

export interface KeyboardListener {
    onKeyPress(e: KeyboardEvent): void;
    onKeyDown(e: KeyboardEvent): void;
}

export class StateMachine {

    static readonly ST_DEFAULT = 0;
    static readonly ST_ADD_NOTE = 1;
    static readonly ST_ADD_LINK = 2;

    stateId:number = StateMachine.ST_DEFAULT;

    saveNote: SaveNote;
    addNote: AddNote;
    addLink: AddLink;

    mouseHandler: SurfaceMouseHandler;
    keyboardListeners: KeyboardListener[] = [];

    init(id: string) {
        surface.init(id);

        this.initInputs();
        this.initToolbar();
    }

    initToolbar() {
        this.addNote = new AddNote('tool-1');
        this.addLink = new AddLink('tool-2');
        this.saveNote = new SaveNote('tool-6');
    }

    initInputs() {
        this.mouseHandler = new SurfaceMouseHandler(surface.c);
        this.mouseHandler.zoomHandler = surface.onZoom;
        this.mouseHandler.panHandler = surface.onPan;

        this.onClick = this.onClick.bind(this);
        this.onDoubleClick = this.onDoubleClick.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onDrag = this.onDrag.bind(this);

        this.mouseHandler.dragHandler = this.onDrag;
        this.mouseHandler.clickHandler = this.onClick;
        this.mouseHandler.doubleClickHandler = this.onDoubleClick;
        this.mouseHandler.mouseUpHandler = this.onMouseUp;
        this.mouseHandler.mouseMoveHandler = this.onMouseMove;

        window.onkeypress = (e: KeyboardEvent) => {
            this.keyboardListeners.forEach(listener => {
                listener.onKeyPress(e);
            })
        }

        window.onkeydown = (e: KeyboardEvent) => {
            if ((e.metaKey || e.ctrlKey) && e.shiftKey) {
                if (e.key == 's') {
                    // TODO: SAVE WORKSPACE
                }
            }

            this.keyboardListeners.forEach(listener => {
                listener.onKeyDown(e);
            })
        }
    }

    stateMachine() {
        surface._draw();
        // TODO: Add InterfaceObjects:  (these an be done in HTML/css)
        //  - Handle Deletion of items
        // TODO: Add tooltip link location handler
        // TODO: add loading popup;
    }

    addNoteTest() {
        const n = new Note(surface, 500, 100, 800, 500);
        n.sections[0].text = MdConst.TTSTING;
        surface.addObject(n);
    }

    _addNote(evt: MouseEvent) {
        const [x, y] = [evt.pageX * SurfaceConst.FIDEL_MULT, evt.pageY * SurfaceConst.FIDEL_MULT];
        const n = new Note(surface, x - surface.rect[0], y - surface.rect[1], 800, 500);

        surface.addObject(n);
        surface.scrollTo(n);

        this.stateId = StateMachine.ST_DEFAULT;
        this.addNote.deactivate();
        Cursor.reset();
        ddraw.event();
    }

    onClick(mouseId: number, evt: MouseEvent) {
        switch (this.stateId) {
            case StateMachine.ST_ADD_NOTE:
                this._addNote(evt);
                break;
            case StateMachine.ST_DEFAULT:
                surface.onStateClick(mouseId, evt);
                break;
        }
    }

    onDoubleClick(mouseId: number, evt: MouseEvent) {
        switch (this.stateId) {
            case StateMachine.ST_ADD_NOTE:

                this.stateId = StateMachine.ST_DEFAULT;
                break;
            case StateMachine.ST_DEFAULT:
                surface.onStateDoubleClick(mouseId, evt);
                break;
        }
    }

    onMouseMove(dxy: number[], evt: MouseEvent) {
        switch (this.stateId) {
            case StateMachine.ST_ADD_NOTE:

                break;
            case StateMachine.ST_DEFAULT:
                surface.onStateMouseMove(dxy, evt);
                break;
        }
    }

    onMouseUp(mouseId: number, evt: MouseEvent) {
        switch (this.stateId) {
            case StateMachine.ST_ADD_NOTE:

                break;
            case StateMachine.ST_DEFAULT:
                surface.onStateMouseUp(mouseId, evt);
                break;
        }
    }

    onDrag(dxy: number[], evt: MouseEvent) {
        switch (this.stateId) {
            case StateMachine.ST_ADD_NOTE:
                break;
            case StateMachine.ST_DEFAULT:
                surface.onStateDrag(dxy, evt);
                break;
        }
    }

    addKeyboardListener(ft: KeyboardListener) {
        if (!this.keyboardListeners.includes(ft)) {
            this.keyboardListeners.push(ft);
        }
    }

    removeKeyboardLister(ft: KeyboardListener) {
        const n = this.keyboardListeners.indexOf(ft);
        if (n >=0) {
            this.keyboardListeners.splice(n, 1);
        }
    }

    getLatestKeyboardListener() {
        return this.keyboardListeners[this.keyboardListeners.length-1];
    }
}

export const stateMachine = new StateMachine();